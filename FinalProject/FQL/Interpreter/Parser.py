

class Parser:
    Source = ""
    SourceCount = 0
    POS = 0
    Line = 1

    AST = None
    ASTS = None

    #Define the language constructs
    Types = ["File", "Path", "Number", "String", "Event", "Const", "Dataset"]
    Selectors = ["From", "INTO"]
    Operators = [":", "/", "+", "=", "-", "*", "%", ">", "<", "<=", ">=", "==", "{", "}", "(", ")", ","]
    StringDelimiter = ["\""]
    Statements = ["SELECT", "DELETE", "UPDATE", "INSERT"]
    Keywords = ["If", "Function", "End", "Else", "While"]
    Delimiter  = [";"]

    
    def __init__(self, Source):
        self.Source = (Source.strip()).replace("\t", " ")
        self.SourceCount = len(self.Source)


    def Parse(self):
        pass


    
    def Ahead(self):
        if(self.POS+1 >= self.SourceCount):
            return False
        else:
            return self.Source[self.POS + 1]



    def getToken(self):
        while(self.POS < self.SourceCount):
            ch = self.Source[self.POS]
            if(ch.isalpha()):
                return self.getWord()
            elif(ch.isdigit()):
                return self.getDigit()
            elif(ch == " " or ch == "\n"):
                if(ch == "\n"):
                    self.Line += 1
                self.POS += 1
                continue
            elif(ch == "/" and self.Ahead() == "*"):
                return self.getComment()
            elif(ch == "\""):
                return self.getString()
            else:
                return self.getOperator()




    def getOperator(self):
        Token = ""
        i = self.POS
        Line = self.Line
        while(i < self.SourceCount):
            ch = self.Source[i]
            if(ch.isalpha() == False and ch.isdigit() == False and ch != " " and 
                ch != "\n" and ch != "\"" ):
                Token += ch
                Ahead = self.Ahead()
                if(Token + Ahead in self.Operators):
                    i+=2
                    Token += Ahead
                    break
                else:
                    i+=1
                    break
            else:
                break

        self.POS = i
        return [Token, "Operator", Line]



    """ This method returns Keyword or identifier like xyz, xyz1, _xyz """
    def getWord(self): 
        Token = ""
        i = self.POS
        Line = self.Line
        while(i < self.SourceCount):
            ch = self.Source[i]
            if((ch.isalpha() or ch.isdigit()) and ch != " " and ch != "\n"):
                Token += ch
                i+=1
            else:
                break
        
        self.POS = i
        if(Token in self.Keywords):
            return [Token, "keyWord", Line]
        else:
            return [Token, "Identifier", Line]



    """ This method returns digits, and even if they contain chars like 123, 123a, 155x"""
    def getDigit(self): 
        Token = ""
        i = self.POS
        Line = self.Line
        while(i < self.SourceCount):
            ch = self.Source[i]
            if((ch.isalpha() == True or ch.isdigit())and ch != " " and ch != "\n"):
                Token += ch
                i+=1
            else:
                break
        
        self.POS = i
        return [Token, "Digit", Line]



    """ This method returns enclosed string, like "string" """
    def getString(self): 
        Token = "\""
        i = self.POS + 1
        Line = self.Line
        while(i < self.SourceCount):
            ch = self.Source[i]
            if(ch != "\"" and ch != "\n"):
                Token += ch
                i+=1
            elif(ch == "\""):
                Token += ch
                i += 1
                break
            else:
                break #nothing
                
        
        self.POS = i
        return [Token, "String", Line]



    def getComment(self):
        Token = "/*"
        i = self.POS + 2
        Line = self.Line
        while(i < self.SourceCount):
            ch = self.Source[i]
            if(ch != "*" and self.Ahead() != "/"):
                if(ch == "\n"):
                    self.Line += 1
                Token += ch
                i+=1
            else:
                Token += "*/"
                break
        
        self.POS = i + 2
        print(Token)
        return [Token, "Comment", Line]
