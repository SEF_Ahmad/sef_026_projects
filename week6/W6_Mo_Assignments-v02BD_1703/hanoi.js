


//@Class steps store state of towers at each step
class steps 
{
    constructor(src, spr, tgt) 
    {
        this.src = src;
        this.spr = spr;
        this.tgt = tgt;
    }
}

 function newsteps(src, spr, trg){
    return {
        source: src,
        spare: spr,
        target: trg
    }
 };

var src_steps = [];
var spr_steps = [];
var tgt_steps = [];

var src_count = 0;
var spr_count = 0;
var tgt_count = 0;


//@Render the function responsible on drawing boxes on each tower for each step
function Render(src, spr, tgt)
{
    var html;
    var div;
    EraseBoxes("source");
    for (let index = 0; index < src.length; index++) 
    {
        AppendBox("source", 9 - src[index], index);        
    }

    EraseBoxes("spare");
    for (let index = 0; index < spr.length; index++) 
    {
        AppendBox("spare", 9 - spr[index], index);        
    }

    EraseBoxes("target");
    for (let index = 0; index < tgt.length; index++) 
    {
        AppendBox("target", 9 - tgt[index], index);        
    }
}


//@EraseBoxes utility function used by Render to delete boxes before drawing them
function EraseBoxes(id)
{
    document.getElementById(id).innerHTML = '<div class="tower"></div>';
}



function getHTML(id)
{
    return document.getElementById(id).innerHTML;
}



function setHTML(id, content)
{
    document.getElementById(id).innerHTML = content;
}


//@AppendBox utility function used by Render() to add boxes to tower
function AppendBox(container_id, size, index)
{
    var html = getHTML(container_id);
    html += '<div class="box size' + size + ' index' + index + ' "> </div>';
    setHTML(container_id, html);
}


//@Hanoi Recursive function that solve the problem and build array of states of towers
function Hanoi(n, source, target, spare)
{
    if(n>0) {
        Hanoi(n-1, source, spare, target);

        target.push(source.pop());


        let s = new Array(src.length);
        let sp = new Array(spr.length);
        let tg = new Array(tgt.length);

        for(let i=0; i < s.length; i++)
        {
            s[i] = src[i];
        }

        for(let i=0; i < sp.length; i++)
        {
            sp[i] = spr[i];
        }

        for(let i=0; i < tg.length; i++)
        {
            tg[i] = tgt[i];
        }

        obj = new steps(s, sp, tg);
        obj2 = newsteps(source, spare, target);
        console.log(obj2);
        src_steps.push(obj);
        src_count++;

        Hanoi(n - 1, spare, target, source);
        
    }
}

var time = 300;
src = ["8", "7", "6", "5", "4", "3", "2", "1"];
spr = [];
tgt = [];
Render(src, spr, tgt);
stps = [];
Hanoi(8, src, spr, tgt);
counter = 0;
var callf = function() {
    if(counter >= src_count) {
        //return;
    }
    else {
        Render(src_steps[counter].src, src_steps[counter].spr, src_steps[counter].tgt);
        document.getElementById("frame").innerHTML=counter;
        counter++;
    }
    setTimeout(callf, time);
};


setTimeout(callf, time);