var ToDo = {
    entries: []
    ,
    getEntries: function ()
    {
        this.entries =  JSON.parse(localStorage.getItem('entries'));
        return this.entries;
    }
    ,
    setEntries: function ()
    {
        localStorage.setItem('entries', JSON.stringify(this.entries));
    }
    ,
    pushEntry: function(entry)
    {
        this.entries.push(entry);
    }
    ,
    popEntry: function(id)
    {
        this.entries.splice(id, 1);
    }
    ,
    isAvailable: function()
    {
        if(localStorage.getItem('entries') != null) {
            return true;
        }
        return false;
    }
    ,
    Render: function(entries)
    {
        let HTML = "";
        for(let i = 0; i < entries.length; ++i)
        {
            HTML += "<div class='item_container'>";
            HTML += "<div class='item'> ";
            HTML += "<div class='item_title'> " + entries[i].title + "</div>";
            HTML += "<div class='item_date'> added: " + entries[i].date + "</div>";
            HTML += "<div class='item_txt'> " + entries[i].txt + "</div>";
            HTML += "</div> ";
            HTML += "<div class='delete'> ";
            HTML += "<button class='del' onclick='deleteEntry(this.id)' id='" + i + "'> X </button>";
            HTML += "</div> ";
            HTML += "</div> ";
        }
        document.getElementById("items").innerHTML = HTML;
    }
};

function deleteEntry(id)
{
    ToDo.popEntry(id);
    ToDo.setEntries();
    ToDo.Render(ToDo.getEntries());
}

if(ToDo.isAvailable()) {
    ToDo.Render(ToDo.getEntries());
}

document.getElementById("btn_add").addEventListener("click", function(){
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth()+1; //January is 0!
    let yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd;
    } 
    if(mm<10){
        mm='0'+mm;
    } 
    let now = dd+'/'+mm+'/'+yyyy;

    let obj = {
        title: document.getElementById("item_title").value
        ,
        date: now
        ,
        txt: document.getElementById("txt_desc").value
    };

    document.getElementById("txt_desc").value = "";
    document.getElementById("item_title").value = "";
    
    ToDo.pushEntry(obj);
    ToDo.setEntries();
    ToDo.Render(ToDo.getEntries());
}); 


