<?php


function Solve($password)
{
    $total = 0;
    $c = count($password);
    for ( $i = 0; $i < $c; $i++ ) 
    {
        $seven = 0;
        $seven += pow(17, $c - $i-1);
        $total +=  $password[$i] * $seven ;
        
    } 
    return $total; 
}



function BuildSet()
{
    $arr = [];
    for ($i = 1; $i <= 26; ++$i)
    {
        array_push($arr, $i);
    }
    return $arr;
}




function WritePass($result, $i)
{
    $chr = "abcdefghijklmnopqrstuvwxyz";
    echo "Password - $i: ";
    for($i = count($result)-1; $i >= 0; --$i)
    {
        echo substr($chr, $result[$i] - 1, 1);
    }
    echo "\n";
}



function Regression($target, $set, $result, $index, $All)
{
    if($index <= 0) {
       if(Solve(array_reverse($result)) == 248410397744610) {
            array_push($All, $result);
            return $All;
       } 
        return $All;
    }
    
    for ($i = 0; $i < count($set); $i++)
    {
        if(($target - $set[$i]) % 17 == 0) {
            array_push($result, $set[$i]);
            $All = Regression(($target - $set[$i]) / 17, $set, $result, $index -1, $All);
            array_pop($result);
        }
        elseif($target - $set[$i] == 0) {
           // Nothing !
        }
    }
    return $All;
}



//$comb = BuildCombinations(12);

$set = BuildSet();
$all = Regression(248410397744610, $set, [], 12, []);
for($i=0; $i < count($all); ++$i)
{
    WritePass($all[$i], $i + 1);
}






/*

function Add($arr)
{
    $result = 0;
    foreach($arr as $a)
    {
        $result += $a;
    }
    return $result;
}




function BuildCombinations($size)
{
    $comb = [];
    for($i=0; $i < $size; ++$i)
    {
        array_push($comb, pow(17, $i));
    }
    return $comb;
}




function BuildCombinations($set)
{
    $comb = [];
    for($i=0; $i < count($set); ++$i)
    {
        $arr = [];
        for($j = 0; $j < 12; $j++)
        {
            array_push($arr, $set[$i] * pow(17, $j));
        }
        array_push($comb, $arr);
    }
    //var_dump($comb);
    return $comb;
}
*/


/*

function Regression($target, $combination, $size, $rc)
{
    $result = 0;
    if($size <= 0) {
        var_dump($rc);
        return $rc; 
    }
    for($i = 0; $i < count($combination); $i++)
    {
        for($j = 0; $j < count($combination[$i]); $j++)
        {
            array_push($rc, $combination[$i][$j]);
            $result = Add($rc);
            if($result == $target) {
                return $rc;
            }
            Regression($target, $combination, $size -1, $rc);
            array_pop($rc);
        }
    }
}



function Regression($target, $combination, $set, $rc)
{
    $result = 0;
    $diff = 0;
    $chosen = [];
    echo "Comb: ". count($combination) . " Set: " . count($set)."\n";
    for($i = 0; $i < count($combination); $i++)
    {
        for($j = 0; $j < count($set); $j++)
        {
            array_push($rc, $combination[$i] * $set[$j]);
            array_push($chosen, $set[$j]);
            $result = $target  -  Add($rc);

            if($result == 0) {
                array_push($rc, $combination[$i] *$set[$j]);
                array_push($chosen, $set[$j]);
                echo "WOW____________";
                return $rc;
            }
            elseif($diff == 0) {
                array_push($rc, $combination[$i] *$set[$j]);
                array_push($chosen, $set[$j]);
            }
            elseif(abs($result) < $diff) {
                $diff = abs($result);
                array_pop($rc);
                array_pop($chosen);
                $array_push($rc, $combination[$i] * $set[$j]);
                array_push($chosen, $set[$j]);
            }
            else {
                array_pop($rc);
                array_pop($chosen);
            }
        }
    }

    var_dump($chosen);
    return $rc; 
}

*/

