<?php
/**
 * Author: Ahmad Issa
 * This file define the base Model class
 */

require_once(__DIR__."/../Database/CrystalQueryBuilder.php");
require_once(__DIR__."/../Exception/ModelException.php");


 class CrystalModel
 {
    protected $Table;
    protected $Querier;
    protected $Schema;
    protected $AutoSet;


    function __Construct()
    {
        $this->Table = strtolower(static::className());
        $this->Querier = new CrystalQueryBuilder();
        $this->Schema = $this->Querier->GetSchema($this->Table);
        $this->AutoSet = array($this->Table.'_id', 'last_update', 'create_date');
        $TSchema = array_diff($this->Schema, $this->AutoSet);
        $this->Schema = array();
        foreach($TSchema as $Ts)
        {
            array_push($this->Schema, $Ts);
        }
        //var_dump($this->Schema);
        //echo "<br/>$this->Table</br>";
    }


    public function className() 
    {
        return get_class($this);
    }


    /**
     * Create inserts spcific row in the table associated with this Model 
     * (by default using user name)
     * @param Values is assoc array that hold table cols as keys and there values as values.
     */
    public function Create($Values) //
    {
        $vals = array();
        foreach($this->Schema as $col)
        {
            if(!array_key_exists($col, $Values)) {
                $e = new ModelException(140, 'Value not exists', debug_backtrace());
                $e->Log();
            }
            array_push($vals, $Values[$col]);
        }
        $this->Querier->InsertInto($this->Table, $this->Schema, $vals)
        ->Execute();
    }


    public function FindById($id)
    {
        $result = $this->Querier->Select('*')
                ->From($this->Table)
                ->Where($this->Table."_id",'=', $id)
                ->Execute();
        return $result;
    }


    public function All()
    {
        $result = $this->Querier->Select('*')
                ->From($this->Table)
                ->get();
        return $result;
    }


    public function DeleteById($id)
    {
        $res = $this->Querier->DeleteFrom($this->Table)
        ->Where($this->Table."_id",'=', $id)
        ->Execute();
    }


    public function DeleteByColumn($Col, $Val)
    {
        $res = $this->Querier->DeleteFrom($this->Table)
        ->Where($Col,'=', $Val)
        ->Execute();
    }


    public function UpdateById($id, $Vals)
    {
        $result = $this->Querier->Update($this->Table, $Vals)
        ->Where($this->Table."_id", '=', $id)
        ->Execute();
    }

    public function GetLastId()
    {
        return $this->Querier->GetLastId();
    }


 }