<?php
/**
 * Author: Ahmad Issa
 * Define CrystalController base class, which implements all controllers in Crystal.
 */


require_once(__DIR__."/../Exception/ControllerException.php");

class CrystalController
{
    protected $MiddleController; //Instance of ordinary controller to be executed, it should have Run method
    protected $RouteArguments;

    function __Construct($RA)
    {
        $this->RouteArguments = $RA;
    }



    protected function Middleware($Controller)
    {
        if(!method_exists($Controller, 'Run')) {
            $e = new ControllerException(115,'Midlleware method Run don\'t exist',
                                                debug_backtrace());
            $e->log();
        }

        $this->MiddleController = $Controller;
        $this->MiddleController->Run();
    }



    protected function View($Name, $Args)
    {
        if(!file_exists(__DIR__."/../../View/".$Name.".php")) {
            $e = new ControllerException(117,'View don\'t exist',
                                                debug_backtrace());
            $e->log();
        }
        $Vars = array_keys($Args);
        require_once(__DIR__."/../../View/".$Name.".php");
    }



    public function JsonRequest()
    {
        return json_decode(file_get_contents('php://input'));
    }
    
}