<?php
/**
 * Author: Ahmad Issa
 * Define Route Exception class, which handle only one route with all its options.
 */

 require_once('CrystalException.php');

 class RouteException extends CrystalException
 {
     public function Log($Text='at Route Module')
     {
        parent::Log($Text);
        exit('Internal Error');
     }
 }