<?php
/**
 * Author: Ahmad Issa
 * Define the base class of Crystal internal exceptions.
 */
//namespace CrystalExceptionM;
//use Exception;
require_once('config.php');

 class CrystalException extends Exception
 {
    protected $Errno;
    protected $Error;
    protected $Trace;


    function __Construct($errno, $err, $trace)
    {
        $this->Errno = $errno;
        $this->Error = $err;
        $this->Trace = $trace;
    }


    public function Log($Text="")
    {
        file_put_contents(ERROR_LOG_FILE,"> ".date('Y-m-d G:i:s').
        "  Error[$this->Errno] $Text: $this->Error\nStack trace::\n", FILE_APPEND);
        foreach($this->Trace as $trace)
        {
            file_put_contents(ERROR_LOG_FILE, "File: ".
             $trace['file']."\n at line ".$trace['line']."\n", FILE_APPEND );
        }
        file_put_contents(ERROR_LOG_FILE, "\n\n", FILE_APPEND );
    }


    public function LogDump($Text)
    {
        file_put_contents(ERROR_LOG_FILE,"> ".date('Y-m-d G:i:s').
        " $Text\n\n", FILE_APPEND);
    }
 }