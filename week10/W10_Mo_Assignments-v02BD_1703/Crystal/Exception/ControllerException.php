<?php
/**
 * Author: Ahmad Issa
 * Define Controller Exception class.
 */

 require_once('CrystalException.php');

 class ControllerException extends CrystalException
 {
     public function Log($Text='at Controller Module')
     {
        parent::Log($Text);
        exit('Internal Error');
     }
 }