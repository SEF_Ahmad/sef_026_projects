<?php
/**
 * Author: Ahmad Issa
 * Define QueryBuilder Exception class
 */

 require_once('CrystalException.php');

 class QueryBuilderException extends CrystalException
 {
    public function Log($Text='')
    {
        parent::Log($Text);
        exit('Internal Error');
    }

    public function LogDump($Text)
    {
        parent::LogDump($Text);
        exit('Internal Error');
    }
 }