<?php
/**
 * Author: Ahmad Issa
 * Define Router static class, which handle all routing Module in Crystal.
 */

require_once(__DIR__.'/Route.php');
require_once(__DIR__."/../Exception/RouteException.php");


class CrystalRouter
{
    public $Routes; //array of registered routes
    protected $Base = 'http://localhost/SEF_026_Ahmad_Issa/week10/W10_Mo_Assignments-v02BD_1703/';
    

    function __Construct()
    {
        $RInstance = new Route($this->Base, 'none', 'ERR');
        $RInstance->Handler = $this->parseController('NotFoundController@Error');
        $this->Routes = array();
        array_push($this->Routes, $RInstance);
    }

    public function Get($Route, $Controller)
    {
        $RInstance = new Route($this->Base, $Route, 'GET');
        $RInstance->Handler = $this->parseController($Controller);
        array_push($this->Routes, $RInstance);
        
    }


    public function Post($Route, $Controller)
    {
        $RInstance = new Route($this->Base, $Route, 'POST');
        $RInstance->Handler = $this->parseController($Controller);
        array_push($this->Routes, $RInstance);
    }


    public function MatchRegisteredRoute($Route)
    {
        foreach($this->Routes as $R)
        {
            if(1 == preg_match($R->RouteMatcher, $Route) && 
                $R->Method == $_SERVER['REQUEST_METHOD']) {
                return $R;
            }
        }
        return $this->Routes[0];
    }


    public function ExtractArguments($RouteInstance, $RequestedRoute)
    {
        $Args = array();
        $Elements = explode('/', $RequestedRoute);
        foreach($RouteInstance->ArgumentsArray as $arg)
        {
            $Args = array_merge($Args, array(
                $arg[0] => $Elements[$arg[1]+1]
            ));
        }
        return $Args;
    }

    private function parseController($Controller)
    {
        $Elements = explode('@', $Controller);
        if(count($Elements) != 2) {
            $e = new RouteException(110,'Bad controller structure',
                                                debug_backtrace());
            $e->log();
        }
        if(!ctype_alpha($Elements[0][0]) || !ctype_alpha($Elements[1][0])) {
            $e = new RouteException(112,'Bad controller or method name',
                                                debug_backtrace());
            $e->log();
        }

        return array($Elements[0], $Elements[1]);
    }


}