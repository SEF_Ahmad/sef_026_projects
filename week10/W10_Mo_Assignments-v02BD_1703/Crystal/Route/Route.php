<?php
/**
 * Author: Ahmad Issa
 * Define Route class, which handle only one route with all its options.
 */

 //namespace Route;
require_once(__DIR__."/../Exception/RouteException.php");

 //use \RouteException;

 class Route 
 {
    protected $Base;
    public $Method;

    //@ArgumentsArray Array of Array arguments names [Arg1, Arg2, ... , ArgN] with their index in route
    public $ArgumentsArray; 
    public $RouteMatcher;
    public $Handler; //Array of structure [cotroller, method]
    public $Route;


    function __Construct($Base, $Route, $method)
    {
        $this->Base = $Base;
        $this->Route = $Route;
        $this->Method = $method;
        $this->ArgumentsArray = array();
        $this->parseRoute($Route);
    }



    public function parseRoute($Route)
    {
        $Route = str_replace(' ', '', $Route);
        $Elements = explode('/', $Route);
        //exit(var_dump($Elements));
        $this->RouteMatcher = '/^';
        $i=0;
        foreach($Elements as $element)
        {
            if($element == "") {
                continue;
            }
    
            if(1 == preg_match('/^\w*\d*$/', $element)) {
                $this->RouteMatcher .= "\/$element";
                
            }
            elseif(1 == preg_match('/^\{\w*\d*\}$/', $element)) {
                $this->RouteMatcher .= '\/\w*\d*';
                $el = str_replace('{', '', $element);
                $el = str_replace('}', '', $el);
                if(!ctype_alpha($el[0])) {
                    $e  = new RouteException(100,'Bad route argument', debug_backtrace());
                    $e->log();
                }
                else {
                  array_push($this->ArgumentsArray, array($el,$i));
                }
            }
            else {
                $e  = new RouteException(105,'Bad route format', debug_backtrace());
                $e->log(); //fatal error
            }
            $i++;
        }
        $this->RouteMatcher .= '($|\/)$/';
    }



 }