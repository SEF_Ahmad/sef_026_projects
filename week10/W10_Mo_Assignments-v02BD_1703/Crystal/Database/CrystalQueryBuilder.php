<?php
/**
 * Author: Ahmad Issa
 * This file define the Query builder that is needed to query the database
 */

 require_once('DBInterface.php');
 require_once(__DIR__."/../Exception/QueryBuilderException.php");

 class CrystalQueryBuilder
 {
    protected $DBI;
    protected $SQL;


    function __Construct()
    {
        $this->DBI = new DBInterface();
        $this->SQL = '';
    }


    public function Select($List)
    {
        $this->SQL = "SELECT $List ";
        return $this;
    }


    public function From($Table)
    {
        $this->SQL .= " From ".$Table." ";
        return $this;
    }


    public function Where($Col, $Condition, $Value)
    {
        $this->SQL .= " Where $Col $Condition $Value ";
        return $this;
    }


    public function And($Col, $Condition, $Value)
    {
        $this->SQL .= " and $Col $Condition $Value ";
        return $this;
    }


    public function Or($Col, $Condition, $Value)
    {
        $this->SQL .= " or $Col $Condition $Value ";
        return $this;
    }


    public function InsertInto($Table, $Cols, $Vals)
    {
        $this->SQL = " INSERT INTO $Table (";
        for($i = 0; $i < count($Cols); $i++)
        {
            $this->SQL .= $Cols[$i];
            if($i != (count($Cols) - 1)) {
                $this->SQL .= ', ';
            }
        } 

        $this->SQL .= ") VALUES (";

        for($i = 0; $i < count($Vals); $i++)
        {
            $this->SQL .= "'".$Vals[$i]."'";
            if($i != (count($Vals) - 1)) {
                $this->SQL .= ', ';
            }
        }
        $this->SQL .= ") ";
        return $this;
    }



    public function DeleteFrom($Table)
    {
        $this->SQL = " DELETE FROM $Table ";
        return $this;
    }


    public function Update($Table, $Vals)
    {
        $this->SQL= "UPDATE $Table SET ";
        $Keys = array_keys($Vals);
        for($i = 0; $i < count($Keys); $i++)
        {
            $this->SQL .= $Keys[$i]." = '".$Vals[$Keys[$i]]."' ";
            if($i < (count($Keys) -1)) {
                $this->SQL .= ", ";
            }
        }
        return $this;
    }

    public function GetSchema($Table)
    {
        $this->SQL = "SELECT * FROM $Table";
        $res = $this->Execute();
        return array_keys($res);
    }


    public function Execute() //for no return queries
    {
        $result = $this->DBI->Execute($this->SQL);
        if($this->DBI->errno != 0) {
            $e = new QueryBuilderException(205, '', debug_backtrace());
            $e->LogDump("at QueryBuilder Module ".$this->DBI->err);
        }
        return mysqli_fetch_assoc($result);
    }


    public function get()
    {
        $result = $this->DBI->Execute($this->SQL);
        if($this->DBI->errno != 0) {
            $e = new QueryBuilderException(205, '', debug_backtrace());
            $e->LogDump("at QueryBuilder Module ".$this->DBI->err);
        }
        return mysqli_fetch_all($result);
    }



    public function GetLastId()
    {
        return $this->DBI->getLastInsert();
    }

    
 }