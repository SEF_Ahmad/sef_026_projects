

function CreateActor()
{
    fn  = document.getElementById('crt_fn').value;
    ln  = document.getElementById('crt_ln').value;
    document.getElementById('crt_fn').value = "";
    document.getElementById('crt_ln').value = "";
    Ajax.Method = 'POST';
    Ajax.Target = 'http://localhost/SEF_026_Ahmad_Issa/week10/W10_Mo_Assignments-v02BD_1703/api/v1/actor/create';
    js = {
        "first_name": fn,
        "last_name": ln
    };
    Ajax.Parameters = JSON.stringify(js);
    Ajax.Ajax(callBackCreate, null, null);
}



function callBackCreate(response)
{
    document.getElementById('crs_response').innerHTML = JSON.stringify(response);
    //alert(JSON.stringify(response));
}



function UpdateActor()
{
    fn  = document.getElementById('updt_fn').value;
    ln  = document.getElementById('updt_ln').value;
    id  = document.getElementById('updt_id').value;
    document.getElementById('updt_fn').value = "";
    document.getElementById('updt_ln').value = "";
    document.getElementById('updt_id').value = "";
    Ajax.Method = 'POST';
    Ajax.Target = 'http://localhost/SEF_026_Ahmad_Issa/week10/W10_Mo_Assignments-v02BD_1703/api/v1/actor/update';
    js = {
        "id": id,
        "first_name": fn,
        "last_name": ln
    };
    Ajax.Parameters = JSON.stringify(js);
    Ajax.Ajax(callBackUpdate, null, null);
}



function callBackUpdate(response)
{
    document.getElementById('crs_response').innerHTML = JSON.stringify(response);
    //alert(JSON.stringify(response));
}





function DeleteActor()
{
    id  = document.getElementById('dlt_id').value;
    document.getElementById('dlt_id').value = "";
    Ajax.Method = 'POST';
    Ajax.Target = 'http://localhost/SEF_026_Ahmad_Issa/week10/W10_Mo_Assignments-v02BD_1703/api/v1/actor/delete';
    js = {
        "id": id,
    };
    Ajax.Parameters = JSON.stringify(js);
    Ajax.Ajax(callBackUpdate, null, null);
}


function ReadActor()
{
    id  = document.getElementById('rd_id').value;
    document.getElementById('rd_id').value = "";
    Ajax.Method = 'GET';
    Ajax.Target = 'http://localhost/SEF_026_Ahmad_Issa/week10/W10_Mo_Assignments-v02BD_1703/api/v1/actor/'+ id +'/read';
    js = {
        "id": id,
    };
    Ajax.Parameters = JSON.stringify(js);
    Ajax.Ajax(callBackUpdate, null, null);
}




 var Ajax = {
    Method: 'GET',
    Target: '',
    Parameters: null,
    Ajax: function(callBack, onError, onClose) {
        if (window.XMLHttpRequest) {
            connection = new XMLHttpRequest();
        }

        connection.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                response = this.responseText;
                console.log(response);
                response = JSON.parse(response);
                if(callBack != null) {
                    callBack(response);
                }
              
            }
            else if(this.readyState == 4 && this.status != 200) {
                if(onError != null) {
                    onError(this.readyState, this.status);
                }
            }

            if(onClose != null && this.readyState == 4) {
                onClose();
            }

            
        };
        connection.open(this.Method, this.Target, true);
        if(this.Method == "POST") {
            //token  = document.getElementsByName('csrf-token')[0].content;
            //connection.setRequestHeader("X-CSRF-Token", token);
        }
        connection.send(this.Parameters);
    }
};