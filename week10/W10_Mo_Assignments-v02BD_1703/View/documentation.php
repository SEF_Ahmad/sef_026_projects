<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="Crystal API">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">   
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="<?='http://'.$_SERVER['SERVER_NAME'] . str_replace('/index.php', '', $_SERVER['PHP_SELF']).'/'?>View/css/main.css?4594545">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand Brand" href="#">Crystal API </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?='http://'.$_SERVER['SERVER_NAME'] . str_replace('/index.php', '', $_SERVER['PHP_SELF']).'/documentation'?>">Documentation <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<?='http://'.$_SERVER['SERVER_NAME'] . str_replace('/index.php', '', $_SERVER['PHP_SELF']).'/demo'?>">Demo <span class="sr-only">(current)</span></a>
                </li>
            </ul>
            </div>
        </nav>
        <div class='row DocCard '>
            <div class='card col-md-8 offset-md-2 TheCard'> 
               <h3 class="DocTitle">Documentation</h3>

               <p class="DocSection">Introduction</p>
               <p class="DocSectionText"> 
                   This is Sakila API manager built using the Crystal MVC framework, 
                   Crystal is minimal MVC framework that mimic Laravel features, and it 
                   support ordinary HTML reponse (like this page) or Json reponse for REST
                   API.
                </p>

                 <div class="col col-md-8 offset-md-2"><hr/></div> 

                <p class="DocSection">Methods</p>
               <p class="DocSectionText"> 
                   This API supports only two methods:
                   <ul class="">
                        <li>POST</li>
                        <li>GET</li>
                    </ul>
                    You should use GET to read or delete data sets. If you want to edit or create items you should 
                    use POST given the right fields.
                </p>


                
                <div class="col col-md-8 offset-md-2"><hr/></div> 

                <p class="DocSection">URL structure</p>
                <p class="DocSectionText"> 
                    API urls are designed to be simple and memorizable. All have the same general structure.
                    <span class="DocSectionQuote">/api/version/item/quantity or Id/action(Create, Read, Update, Delete)</span>
                    <br/>
                    <span class="DocSectionText">So for example to read all actors you should access path:</span>
                    <br/>
                    <span class="DocSectionQuote">/api/v1/actor/all/read</spane>

                </p>

            </div>


        </div>
        <script src="<?='http://'.$_SERVER['SERVER_NAME'] . str_replace('/index.php', '', $_SERVER['PHP_SELF']).'/'?>View/js/main.js"></script>
    </body>
</html>

