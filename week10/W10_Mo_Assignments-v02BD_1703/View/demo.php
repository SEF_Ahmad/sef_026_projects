<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="Crystal API">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">   
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="<?='http://'.$_SERVER['SERVER_NAME'] . str_replace('/index.php', '', $_SERVER['PHP_SELF']).'/'?>View/css/main.css?4594545">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand Brand" href="#">Crystal API </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?='http://'.$_SERVER['SERVER_NAME'] . str_replace('/index.php', '', $_SERVER['PHP_SELF']).'/documentation'?>">Documentation <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item active">
                    <a class="nav-link" href="<?='http://'.$_SERVER['SERVER_NAME'] . str_replace('/index.php', '', $_SERVER['PHP_SELF']).'/demo'?>">Demo <span class="sr-only">(current)</span></a>
                </li>
            </ul>
            </div>
        </nav>
        <div class='row DocCard '>
            <div class='card col-md-8 offset-md-2 TheCard'> 
                <h3 class="DocTitle">Demo Actor</h3>
                <p class="DemoSection offset-md-1">Create</p>
                <div class="row DemoSection">
                    <input id="crt_fn" type="text" placeholder="first name" class="form-control demoBox col col-md-8 offset-md-2"/>
                    <br/>
                    <input id="crt_ln" type="text" placeholder="last name" class="form-control demoBox col col-md-8 offset-md-2"/>
                    <br/>
                    <button id="crt_btn" onclick="CreateActor()" class="btn btn-dark btnDemo col col-md-4 offset-md-4" data-toggle="modal" data-target="#MyModal">Create</button>
                </div>

                <div class="col col-md-8 offset-md-2"><hr/></div>
                <p class="DemoSection offset-md-1">Update</p>
                <div class="row DemoSection">
                    <input id="updt_id" type="text" placeholder="id" class="form-control demoBox col col-md-8 offset-md-2"/>
                    <br/>
                    <input id="updt_fn" type="text" placeholder="first name" class="form-control demoBox col col-md-8 offset-md-2"/>
                    <br/>
                    <input id="updt_ln" type="text" placeholder="last name" class="form-control demoBox col col-md-8 offset-md-2"/>
                    <br/>
                    <button id="updt_btn" onclick="UpdateActor()" class="btn btn-dark btnDemo col col-md-4 offset-md-4" data-toggle="modal" data-target="#MyModal">Update</button>
                </div>
                <div class="col col-md-8 offset-md-2"><hr/></div> 


                <p class="DemoSection offset-md-1">Delete</p>
                <div class="row DemoSection">
                    <input id="dlt_id" type="text" placeholder="id" class="form-control demoBox col col-md-8 offset-md-2"/>
                    <br/>
                    <button id="dlt_btn" onclick="DeleteActor()" class="btn btn-dark btnDemo col col-md-4 offset-md-4" data-toggle="modal" data-target="#MyModal">Delete</button>
                </div>
                <div class="col col-md-8 offset-md-2"><hr/></div> 



                 <p class="DemoSection offset-md-1">Read</p>
                <div class="row DemoSection">
                    <input id="rd_id" type="text" placeholder="id" class="form-control demoBox col col-md-8 offset-md-2"/>
                    <br/>
                    <button id="rd_btn" onclick="ReadActor()" class="btn btn-dark btnDemo col col-md-4 offset-md-4" data-toggle="modal" data-target="#MyModal">Read</button>
                </div>
                <div class="col col-md-8 offset-md-2"><hr/></div> 




                </div>


            </div>

                <div class="modal fade" tabindex="-1" role="dialog" id="MyModal" aria-labelledby="MyModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Crystal Response</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p class="modalText" id="crs_response"></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                        </div>
                    </div>
                </div>


        </div>
        <script src="<?='http://'.$_SERVER['SERVER_NAME'] . str_replace('/index.php', '', $_SERVER['PHP_SELF']).'/'?>View/js/main.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    </body>
</html>

