<?php

/**
 * Author: Ahmad Issa
 * This file defines user's routes
 */


 $Route->Get('/', 'DocController@Show');

 $Route->Get('/test', 'TestController@test');

 $Route->Get('/documentation', 'DocController@Show');

 $Route->Get('/demo', 'DemoController@Show');

 $Route->Get('/api/v1/actor/all/read', 'ActorController@ShowAll');

 $Route->Get('/api/v1/actor/{id}/read', 'ActorController@Show');

 $Route->Post('/api/v1/actor/update', 'ActorController@Update');

 $Route->Post('/api/v1/actor/create', 'ActorController@Create');

 $Route->Post('/api/v1/actor/delete', 'ActorController@Delete');


 $Route->Get('/api/v1/customer/all/read', 'CustomerController@ShowAll');

 $Route->Get('/api/v1/customer/{id}/read', 'CustomerController@Show');

 $Route->Post('/api/v1/customer/update', 'CustomerController@Update');

 $Route->Post('/api/v1/customer/create', 'CustomerController@Create');

 $Route->Post('/api/v1/customer/delete', 'CustomerController@Delete');
