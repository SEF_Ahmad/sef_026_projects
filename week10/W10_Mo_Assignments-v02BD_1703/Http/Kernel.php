<?php
/**
 * - Author: Ahmad Issa
 * This is the core connection point, all routes are redirected here, and then processed 
 * and sent to controllers
 */

/*$Request = str_replace('/SEF_026_Ahmad_Issa/week10/W10_Mo_Assignments-v02BD_1703',
                        '', $_SERVER['REQUEST_URI']);*/



$Request = explode('/W10_Mo_Assignments-v02BD_1703', $_SERVER['REQUEST_URI'])[1];
require_once(__DIR__.'/../Crystal/Route/CrystalRouter.php');
//require_once('');


$Route = new CrystalRouter();
require_once(__DIR__.'/../Route/Web.php');
$path = $Route->MatchRegisteredRoute($Request);
$Args = $Route->ExtractArguments($path, $Request);
require_once(__DIR__.'/../Crystal/Controller/CrystalController.php');

require_once(__DIR__.'/Controller/'.$path->Handler[0].".php");
$controller = new $path->Handler[0]($Args);
$controller->{$path->Handler[1]}();


