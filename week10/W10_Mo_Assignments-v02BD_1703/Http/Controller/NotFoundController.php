<?php
/**
 * Author: Ahmad Issa
 * This is standard controller responsible on hanlding 404 erors
 */


 class NotFoundController extends CrystalController
 {
    public function Error()
    {
        $this->View('404', array());
    }
 }