<?php
/**
 * Author: Ahmad Issa
 * This is Actor controller
 */

 require_once(__DIR__.'/../Model/Actor.php');
 require_once(__DIR__.'/../Model/Film_Actor.php');

 class ActorController extends CrystalController
 {

    public function ShowAll()
    {
        $a = new Actor();
        $result = $a->All();
        
        echo json_encode(array(
           'Key' => '',
            'data' => $result
        ));
        //$this->View('documentation', array());
    }


    public function Show()
    {
        $a = new Actor();
        $result = $a->FindById($this->RouteArguments['id']);
        
        echo json_encode(array(
           'Key' => '',
            'data' => $result
        ));
    }


    public function Update()
    {
        $Json = $this->JsonRequest();
        $a = new Actor();
        $a->UpdateById($Json->id, array(
            'first_name' => $Json->first_name,
            'last_name' => $Json->last_name
        ));
        echo json_encode(array(
            'Key' => '',
             'data' => 'true'
         ));
    }


    public function Delete()
    {
        $Json = $this->JsonRequest();
        $a = new Actor();
        $fa = new Film_Actor();
        $fa->DeleteByColumn('actor_id', $Json->id); //Remove foriegn depe
        $a->DeleteById($Json->id);
        echo json_encode(array(
            'Key' => '',
             'data' => 'true'
         ));
    }


    public function Create()
    {
        $Json = $this->JsonRequest();
        $a = new Actor();
        $a->Create(array(
            'first_name' => $Json->first_name,
            'last_name' => $Json->last_name
        ));

        $id = $a->GetLastId();
        $result = $a->FindById($id);
        echo json_encode(array(
            'Key' => '',
             'data' => $result
         ));
    }

 }