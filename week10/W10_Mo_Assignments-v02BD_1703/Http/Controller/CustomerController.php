<?php
/**
 * Author: Ahmad Issa
 * This is Actor controller
 */

 require_once(__DIR__.'/../Model/Customer.php');
 require_once(__DIR__.'/../Model/Payment.php');
 require_once(__DIR__.'/../Model/Rental.php');

 class CustomerController extends CrystalController
 {

    public function ShowAll()
    {
        $c = new Customer();
        $result = $c->All();
        
        echo json_encode(array(
           'Key' => '',
            'data' => $result
        ));
        //$this->View('documentation', array());
    }


    public function Show()
    {
        $c = new Customer();
        $result = $c ->FindById($this->RouteArguments['id']);
        
        echo json_encode(array(
           'Key' => '',
            'data' => $result
        ));
    }


    public function Update()
    {
        $Json = $this->JsonRequest();
        $c = new Customer();
        $a->UpdateById($Json->id, array(
            'store_id' => $Json->store_id,
            'first_name' => $Json->first_name,
            'last_name' => $Json->last_name,
            'email' => $Json->email,
            'address_id' => $Json->address_id,
            'active' => $Json->active
        ));
        echo json_encode(array(
            'Key' => '',
             'data' => 'true'
         ));
    }


    public function Delete()
    {
        $Json = $this->JsonRequest();
        $c = new Customer();
        $p = new Payment();
        $p->DeleteByColumn('customer_id', $Json->id); //Remove foriegn depe
        $r = new Rental();
        $r->DeleteByColumn('customer_id', $Json->id); //Remove foriegn depe
        $c ->DeleteById($Json->id);
        echo json_encode(array(
            'Key' => '',
             'data' => 'true'
         ));
    }


    public function Create()
    {
        $Json = $this->JsonRequest();
        $c = new Customer();
        $c ->Create(array(
            'store_id' => $Json->store_id,
            'first_name' => $Json->first_name,
            'last_name' => $Json->last_name,
            'email' => $Json->email,
            'address_id' => $Json->address_id,
            'active' => $Json->active
        ));

        $id = $c->GetLastId();
        $result = $c->FindById($id);
        echo json_encode(array(
            'Key' => '',
             'data' => $result
         ));
    }

 }