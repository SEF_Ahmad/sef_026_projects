<?php
/**
 * Author: Ahmad Issa
 * This is documentation controller
 */


 class DocController extends CrystalController
 {
    public function Show()
    {
        $this->View('documentation', array());
    }
 }