@extends("layout/layout")

@section("content")

<h2 id="hTitle">Add Blog</h2>

<div class="row">
    <div class="col col-md-3"></div>
    <form class="col col-md-6" name="frmWrite" id="frmWrite" action="writeAction" method="Post">
            {!! csrf_field() !!}
        <div class="col col-md-4"></div>

        <div class="form-group">
            <input id="inTitle" name="inTitle" class="txtbox form-control" type="text" placeholder="Title"/>
        </div>

        <div class="form-group">
            <textarea id="txtArea" name="txtArea" class="txtbox form-control" placeholder="Text" rows="7"></textarea>
        </div>

        <div class="card" id="crdChk">
            <div class="row">
            @foreach($tags as $tag)
                <div class="form-check form-check-inline ">
                    <input type="checkbox" name="tag{{$tag["id"]}}" class="form-check-input">
                    <label class="form-check-label chk">
                        {{$tag["text"]}}
                    </label>
                </div>
            @endforeach 
            </div>
        </div>

        <div class="row">
            <div class="col col-md-4"></div>
            <button type="submit" id="btnAdd" class="btn btn-success"> Add </button>&nbsp;&nbsp;
            <button type="button" id="btnCancel" class="btn btn-dark">Cancel</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    document.getElementById("btnCancel").addEventListener('click', function() {
        window.location = "{{url("/articles")}}";
    });
</script>

@endsection