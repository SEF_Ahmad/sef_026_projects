@extends ('layout/layout')

@section('content')
    <div class="row">
        <div class="col col-md-3"></div>
        <div class="col col-md-6">
            <article>
                <div class="articleTitle" ><h2>{{$article['title']}}</h2></div>
                <div class="articleText">{{$article['text']}}</div>

                <div class="articleFooter">

                </div>
                
            </article>
            <div class="row">
                    <div class="articleFooter col col-md-4"> 
                        Ceated at: {{$article['created_at']}}
                    </div>

                    @if($article["updated_at"] != null)
                        <div class="articleFooter col col-md-4">
                                Updated at: {{$article['updated_at']}}
                        </div>
                    @endif

                </div>
        </div>
    </div>
@endsection