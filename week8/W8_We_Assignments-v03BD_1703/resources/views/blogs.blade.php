
@extends ('layout/layout')

@section('content')
    @foreach($articles as $article)
        <div class="row">
            <div class="col col-md-3"></div>
            <div class="col col-md-6">
                <div class="card">
                    <div class="card-header articlesTitle card-primary" >
                        <a href="{{url("/article/".$article['id'])}}">
                            {{$article['title']}}
                        </a>
                    </div>
                    <div class="card-body">{{$article['text']}}</div>

                    <div class="card-footer articlesFooter">
                        <div class="row">

                            <div class="col col-md-4"> 
                                Ceated at: {{$article['created_at']}}
                            </div>
                            
                            @if($article["updated_at"] != null)
                                <div class="col col-md-4">
                                        Updated at: {{$article['updated_at']}}
                                </div>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection