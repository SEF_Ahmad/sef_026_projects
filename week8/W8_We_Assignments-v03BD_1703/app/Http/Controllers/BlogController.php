<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    //

    public function getBlogs()
    {
        $blogs = \App\blog::all();
        return view('blogs', ['articles' => $blogs]);
    }


    public function getBlog($blog)
    {
        $article = \App\blog::where('id', $blog)->get();
        if(count($article) == 0) {
            return view('notfound');
        }
        return view('blog', ['article' => $article[0]]);
    }
}

