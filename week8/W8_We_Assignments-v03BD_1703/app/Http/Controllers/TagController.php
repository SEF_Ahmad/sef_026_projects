<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class TagController extends Controller
{
    //

    public function getAll()
    {
        if (!Auth::check()) {
            return view('auth/login');
        }
        $tags = \App\tag::all();
        
        return view('write', ['tags' => $tags]);
    }
}
