<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BlogController@getBlogs');



Route::get('/articles', 'BlogController@getBlogs');

Route::get('/article/{blog}', 'BlogController@getBlog');

Route::get('/write', 'TagController@getAll');

Route::post('/writeAction', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'BlogController@getBlogs');
