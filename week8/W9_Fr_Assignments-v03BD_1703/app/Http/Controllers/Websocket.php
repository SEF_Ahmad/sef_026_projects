<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Validator;

class Websocket extends Controller
{
    //
    private $IdentityMessage;

    function __Construct()
    {
        $this->IdentityMessage = 'Hi it is Me';
    }

    public function AuthMessage(Request $request)
    {
        $validator = Validator::make($request->all(), 
                                                [
                                                'data' => 'required']);
                                                     
            if ($validator->fails()) {
               return (new \App\Exceptions\WsException)->ParametersFailed();
            }
            else {
                $req = json_decode($request->get('data'));
                $privKey = \App\User::find($req->user_id)->private_key;
                $publicKey = \App\User::find($req->user_id)->public_key;
                $encrypted = base64_decode(urldecode($req->identity));
                openssl_private_decrypt($encrypted, $decrypted, $privKey);
                if($decrypted != $this->IdentityMessage) {
                    error_log('identiity fail');
                   return (new \App\Exceptions\WsException)->AuthFailed();
                }

                $encrypted = base64_decode(urldecode($req->message));
                openssl_private_decrypt($encrypted, $decrypted, $privKey);
               $msg =  $this->StoreMessage($req->user_id, $decrypted, $req->channel_id);
                return $this->Success($msg, $req->user_id);
            }
    }


    private function StoreMessage($User, $Message, $Channel)
    {
        $msg = new \App\Message();
        $msg->user_id = $User;
        $msg->message = $Message;
        $msg->channel_id = $Channel;
        $msg->save();
        return $msg;
    }


    
    private function Success($msg, $user)
    {
        return response()->json(array(
            'response'=> $msg->message,
            'user_name' => \App\User::find($user)->name,
            'created' => $msg->created_at->format('m/d/Y H:i:s') ,
            'Errorno' => 0,
        ), 200);
    }


    public function getChannels()
    {
        return response()->json(array(
            'response'=> \App\Channel::all(),
            'Errorno' => 0,
        ), 200);
    }


    public function MessageAuth(Request $request)
    {

    }
}
