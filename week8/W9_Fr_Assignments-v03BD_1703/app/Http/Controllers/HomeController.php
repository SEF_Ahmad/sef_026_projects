<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = \App\Channel::find(1)->messages->sortBy('created_at');
        $channels = \App\Channel::all();
        return view('home', [
            'Messages' => $messages,
            'Channels' => $channels
            ]);
    }


    public function Refresh(Request $request)
    {
        error_log(($request->get('channel_id')));
       /* $validator = Validator::make($request->all(), 
                    [
                        'channel_id' => 'required',
                    ]
        );  */
            
            if (false) { //$validator->fails()
                error_log('faiiiiiil');
                return (new \App\Exceptions\WsException)->ParametersFailed();
            }
            else {
                error_log('pppppppp');
                return response()->json(array(
                    'response'=>  \App\Message::with('user')->where('channel_id', $request->get('channel_id'))->get()
                    ,
                    'Errorno' => 0,
                ), 200);
            }
    }
}
