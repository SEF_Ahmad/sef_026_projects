<?php

namespace App\Exceptions;

use Exception;

class WsException extends Exception
{

    public function AuthFailed()
    {
        return response()->json(array(
            'response'=> 'Authentication of this user failed ',
            'Errorno' => 100,
        ), 200);
    }

    public function ParametersFailed()
    {
        return response()->json(array(
            'response'=> 'Too few parameters sent',
            'Errorno' => 105,
        ), 200);
    }
}
