<?php 

require_once('Websocket.php');
use Devristo\Phpws\Server\WebSocketServer;

class WsInterface
{
    public $Socket;
    public $Port;
    private $Channels; //Array
    public $loop;
    public $logger;
    public $writer;
    public $server;
    public $router;
    private $DBI;


    function __Construct($Ch, $DB)
    {
        $this->Channels = $Ch;
        $this->DBI = $DB;
        $this->loop = \React\EventLoop\Factory::create();
        $this->logger = new \Zend\Log\Logger();
        $this->writer = new Zend\Log\Writer\Stream("php://output");
        $this->logger->addWriter($this->writer);
        $this->server = new WebSocketServer("tcp://0.0.0.0:12390", $this->loop,
                                                                $this->logger);
        $this->router = new \Devristo\Phpws\Server\UriHandler\ClientRouter(
            $this->server,
             $this->logger);
    }


    public function StartChannels()
    {
        foreach($this->Channels as $channel)
        {
            $Socket =  new Websocket($this->logger);
            $Socket->setFields($this, $channel);
            $this->router->addRoute('#^/'.$channel->name.$channel->id.'/*$#i',
                                                                    $Socket);   
        }
    }


    public function onMessage ($connections, $user, $msg, $channel)
    {
        $this->logger->notice("Broadcasting " . strlen($msg->getData()) . " bytes");
        $encrypted="";
        $response = json_decode($msg->getdata());
        $decrypted = $this->DBI->AuthMessage(['data' => $msg->getData()]);
        openssl_public_encrypt($decrypted, $encrypted, $channel->public_key);
        var_dump($decrypted);
        foreach($connections as $client)
        {
            $client->sendString(base64_encode($encrypted));
        }
    }


    public function onConnect($connections, $user, $channel)
    {
        foreach($connections as $client)
        {
            $response = json_encode(array(
                'reponse' => "Server {$user->getId()} joined the chat: ",
            ));
            
            $client->sendString($response);
        }
    }


    public function Start()
    {
        $this->server->bind();
        $this->loop->run();
    }


}