<?php 

require_once('DBInterface.php');
require_once('WsInterface.php');

class WsSlackServer
{
    private $DBI;
    private $WSI;


    function __Construct()
    {
        $this->DBI = new DBInterface('http://localhost/SEF_026_Ahmad_Issa/week8/W9_Fr_Assignments-v03BD_1703/public/');
    }

    public function Fire()
    {
        $Channels = $this->DBI->getChannels();
        $this->WSI = new WsInterface($Channels, $this->DBI);
        $this->WSI->StartChannels();
        $this->WSI->Start();
    }


}

$Server = new WsSlackServer();
$Server->Fire();