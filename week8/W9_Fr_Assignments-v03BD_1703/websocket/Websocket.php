<?php 

date_default_timezone_set('UTC');

require_once("vendor/autoload.php");

use Devristo\Phpws\Framing\WebSocketFrame;
use Devristo\Phpws\Framing\WebSocketOpcode;
use Devristo\Phpws\Messaging\WebSocketMessageInterface;
use Devristo\Phpws\Protocol\WebSocketTransportInterface;
use Devristo\Phpws\Server\IWebSocketServerObserver;
use Devristo\Phpws\Server\UriHandler\WebSocketUriHandler;
use Devristo\Phpws\Server\WebSocketServer;


class Websocket extends WebSocketUriHandler
{

    private $WsI;
    private $Channel;
    
    public function setFields($Inter, $ch)
    {
        $this->WsI = $Inter;
        $this->Channel = $ch;
    }

    public function onConnect(WebSocketTransportInterface $user)
    {

        $this->WsI->onConnect($this->getConnections(), $user, $this->Channel);
        
        /*foreach($this->getConnections() as $client)
        {
            $response = json_encode(array(
                'reponse' => "Server {$user->getId()} joined the chat: ",
            ));
            
            $client->sendString($response);
        }*/
    }


    public function onMessage(WebSocketTransportInterface $user, WebSocketMessageInterface $msg)
    {
        $this->WsI->onMessage($this->getConnections(), $user, $msg, $this->Channel);
        /*$this->logger->notice("Broadcasting " . strlen($msg->getData()) . " bytes");

        foreach($this->getConnections() as $client)
        {
            $client->sendString("User {$user->getId()} said: ".$msg->getData());
        }*/
    }


    public function onDisconnect(WebSocketTransportInterface $user)
    {
        
    }
    private function createKeys()
    {
        $config = array(
            "digest_alg" => "sha512",
            "private_key_bits" => 4096,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );
           
        // Create the private and public key
        $res = openssl_pkey_new($config);
        
        // Extract the private key from $res to $privKey
        openssl_pkey_export($res, $privKey);
        
        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);
        
        $pubKey = $pubKey["key"];

        return (['public' => $pubKey, 'private' => $privKey]);
        //$data = 'plaintext data goes here';
        
        //openssl_public_encrypt($data, $encrypted, $pubKey);
        
        //openssl_private_decrypt($encrypted, $decrypted, $privKey);
    }
}