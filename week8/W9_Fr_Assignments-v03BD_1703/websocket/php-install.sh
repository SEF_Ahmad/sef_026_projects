 sudo apt-get install gcc libmysqlclient-dev libxml2-dev
  wget --trust-server-names http://museum.php.net/php5/php-5.4.15.tar.bz2
  tar xjf php-5.4.15.tar.bz2
  cd php-5.4.15
  ./configure --prefix=$PWD/installdir --enable-bcmath --with-mysql
  make install
  cd -
