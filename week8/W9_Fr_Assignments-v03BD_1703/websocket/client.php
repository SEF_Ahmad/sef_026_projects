<?php

require_once("vendor/autoload.php");                // Composer autoloader

$loop = \React\EventLoop\Factory::create();

$logger = new \Zend\Log\Logger();
$writer = new Zend\Log\Writer\Stream("php://output");
$logger->addWriter($writer);

$client = new \Devristo\Phpws\Client\WebSocket("ws://localhost:12380", $loop, $logger);

$client->on("request", function($headers) use ($logger){
	echo 'wow';
    $logger->notice("Request object created!");
});

$client->on("handshake", function() use ($logger) {
    $logger->notice("Handshake received!");
});

$client->on("connect", function($headers) use ($logger, $client){
    $logger->notice("Connected!");
    $client->send("Hello world!");
});

$client->on("message", function($message) use ($client, $logger){
    $logger->notice("Got message: ".$message->getData());
	echo 'wow'; 
    $client->close();
});

$client->open()->then(function() use($logger, $client){
    $logger->notice("We can use a promise to determine when the socket has been connected!");
});
$loop->run();
