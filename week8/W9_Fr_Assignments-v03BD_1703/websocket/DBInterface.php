<?php 

class DBInterface
{
    public $BaseAddress;


    public function __Construct($Base)
    {
        $this->BaseAddress = $Base;
    }

    private function Http_POST($Address, $PostFields)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $Address);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $PostFields);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $Response = curl_exec($curl);
        var_dump($Response);
        curl_close($curl);
        return $Response;
    }

    private function Http_GET($Address) //overloaded, make GET method instead of post
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $Address);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $Response = curl_exec($curl);
        curl_close($curl);
        return $Response;
    }


    public function getChannels()
    {
       return json_decode($this->Http_GET($this->BaseAddress.'/channels/get'))->response;
    }

    public function AuthMessage($msg)
    {
        return ($this->Http_POST($this->BaseAddress.'user/message/create', $msg));
    }
}




