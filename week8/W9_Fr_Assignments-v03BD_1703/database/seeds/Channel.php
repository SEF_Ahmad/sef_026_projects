<?php

use Illuminate\Database\Seeder;

class Channel extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = $this->createKeys();
        $seed = new \App\Channel();
        $seed->name = 'General';
        $seed->port = '12390';
        $seed->user_id = 1;
        $seed->public_key = $keys['public'];
        $seed->private_key = $keys['private'];
        $seed->save();
        //-------------------------------------
        $keys = $this->createKeys();
        $seed = new \App\Channel();
        $seed->name = 'Batch6';
        $seed->port = '12390';
        $seed->user_id = 1;
        $seed->public_key = $keys['public'];
        $seed->private_key = $keys['private'];
        $seed->save();
    }

    private function createKeys()
    {
        $config = array(
            "digest_alg" => "sha512",
            "private_key_bits" => 1024,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );
           
        // Create the private and public key
        $res = openssl_pkey_new($config);
        
        // Extract the private key from $res to $privKey
        openssl_pkey_export($res, $privKey);
        
        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);
        
        $pubKey = $pubKey["key"];

        return (['public' => $pubKey, 'private' => $privKey]);
        //$data = 'plaintext data goes here';
        
        //openssl_public_encrypt($data, $encrypted, $pubKey);
        
        //openssl_private_decrypt($encrypted, $decrypted, $privKey);
    }
}
