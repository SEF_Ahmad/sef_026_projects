<?php

use Illuminate\Database\Seeder;

class Message extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seed = new \App\Message();
        $seed->message = 'Hi';
        $seed->user_id = 1;
        $seed->channel_id = 1;
        $seed->save();
    }
}
