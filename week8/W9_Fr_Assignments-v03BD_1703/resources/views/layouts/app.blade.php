<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    

    <!-- Fonts
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    
    -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">   
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light  fixed-top insta-nav">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <i class="fab fa-slack  fa-w-14 fa-2x">
                        <span class="txtLogo"> {{ config('app.name', 'Slack') }} </span>
                    </i>

                    
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="mainContent">
            @yield('content')
        </main>
    </div>

    <footer class="footer">
        <div class="row msgBlock">
            <div class="col col-md-3 paddSection" ></div>
            <div class="col col-md-8">
                <div class="row">
                    <button class="btn btn-success msgBtn col-md-1" style="margin-left:10px;" id="Go"><i class="fa fa-plus"></i></button>
                    <input id="MsgBox" name="MsgBox" placeholder="Message" type="text" class="form-control msgBox col-md-10"/>
                </div>
            </div>
        </div> 
    </footer> 
       
      
          <div class="modal fade" id="mdlUpload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="mdlUpload">New Post</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <form id="exampleform" name="fileinfo" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col col-md-8 offset-md-2" id="picUpload">
                        <label for="picture" id="mdlCamera"><i class="fa fa-camera fa-5x"></i>
                            <input id="picture" name="picture" accept=".png,.jpg,.jpeg" type="file" class="form-control-file" aria-describedby="fileHelp">
                        </label>
                        <div class="col-md-12">
                                <img class="img-fluid" id="imgChosen" />
                            </div>
                        </div>
                    
            
                    <div class="col col-md-12">
                            <textarea name="txtDescription" id="txtDescription" rows="5" placeholder="caption" class="form-control instaBox"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 offset-md-2">
                        <button type="submit" id="btnPost" class="btn btn-primary btn-block">Post</button>
                    </div>
                    
                </div>
              </div>
            </div>
          </div>

    <!-- Scripts 
     src="http://code.jquery.com/jquery-3.3.1.min.js"
              crossorigin="anonymous"></script>
              <script src="{{ asset('js/main.js') }}" defer></script>
    -->
   
    <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>	 
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    
</body>
</html>
