@extends('layouts.app')

@section('content')
<div class="row">
<div class="postContainer"  id="postContainer"> 
    
    <div class="col col-md-3 channels" style="padding: 0;">
            <div class="col channelsHead">
                <strong>Channels</strong>
                <br/>
                <small><i id="auth" class="fas fa-circle faBullet" data-id="{{Auth::user()->id}}"></i> {{Auth::user()->name}}</small>
            </div>
           @foreach($Channels as $channel)
                @if($channel->id == 1)
                    <div id="channel{{$channel->id}}" name="{{$channel->name}}" onclick="ChangeChannel(this.id)" data-key="{{$channel->private_key}}" class="col channelTitle channelActive" data-id="{{$channel->id}}">{{$channel->name}}</div>
                @else
                    <div id="channel{{$channel->id}}" onclick="ChangeChannel(this.id)" name="{{$channel->name}}" data-key="{{$channel->private_key}}" class="col channelTitle" data-id="{{$channel->id}}">{{$channel->name}}</div>
                @endif
           @endforeach
    </div>

    <div id="chats" class="col col-md-9 chats">
            <h2><strong>Chat</strong></h2>
            <hr/>
            @foreach($Messages as $message)
            <?php $author = \App\User::find($message->user_id)?>
            <p>
                @if($author->id == Auth::user()->id)
                    <span class="fa fa-user faUser" id="msgUser" data-id="{{$author->id}}">
                @else
                    <span class="fa fa-user fanotUser" id="msgUser" data-id="{{$author->id}}">
                @endif
                    {{$author->name}}:
                </span>
                <span class="messageTxt">
                    {{$message->message}} &nbsp;<small class="smallText">{{$message->created_at}}</small>
                </span>
            </p>
            @endforeach


    </div>
    </div>
    
</div>


        <label class="hidden" id="public">{{Auth::user()->public_key}}</label>
        <label class="hidden" id="private">{{Auth::user()->private_key}}</label>


        <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>	 
        <script src="{{ asset('js/jsencrypt.min.js') }}" defer></script>
        <script src="{{ asset('js/Websocket.js') }}" defer></script>

        <script type="text/javascript">
            var user_id = "{{Auth::user()->id}}";
            var RefreshChannel = "{{url('/channels/refresh')}}";
        </script>
@endsection
