window.onload = function() {
    document.getElementById('picture').addEventListener('change', function(evt){
        var files = evt.target.files;
        src = document.getElementById('picture').value;
        document.getElementById('imgChosen').src = src;
    }, false);
};


var Ajax = {
    Method: 'GET',
    Target: '',
    Parameters: null,
    Ajax: function(callBack, onError, onClose) {
        if (window.XMLHttpRequest) {
            connection = new XMLHttpRequest();
        }

        connection.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                response = this.responseText;
                console.log(response);
                response = JSON.parse(response);
                if(callBack != null) {
                    callBack(response);
                }
              
            }
            else if(this.readyState == 4 && this.status != 200) {
                if(onError != null) {
                    onError(this.readyState, this.status);
                }
            }

            if(onClose != null && this.readyState == 4) {
                onClose();
            }

            
        };
        connection.open(this.Method, this.Target, true);
        if(this.Method == "POST") {
            token  = document.getElementsByName('csrf-token')[0].content;
            connection.setRequestHeader("X-CSRF-Token", token);
        }
        connection.send(this.Parameters);
    }
};



//@callBackModal use to support Ajax call for post Modal
function callBackModal(response) 
{
   if(response.Errorno != 0) {
       alert(response.response);
   }
   else {
       window.location = postURL + '\/' + response.response;
   }
}

//@onErrorModal use to support Ajax call for post Modal
function onErrorModal(ready, status)
{
    alert("Error Occured on Server!\nReady: " + ready + "\nStatus: " + status);

}

//@onCloseModal use to support Ajax call for post Modal
function onCloseModal()
{
    document.getElementById('btnPost').innerHTML = "Post";
    return;
}



//@readURL load image on modal for preview
function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();
    
      reader.onload = function(e) {
        document.getElementById('imgChosen').src =  e.target.result;
      }
    
      reader.readAsDataURL(input.files[0]);
    }
}
    

