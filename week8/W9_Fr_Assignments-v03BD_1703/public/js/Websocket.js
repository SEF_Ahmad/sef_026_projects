        var ch_id = document.getElementsByClassName('channelActive')[0].dataset.id;
        var ch_name = document.getElementsByClassName('channelActive')[0].getAttribute('name');
        var host = "ws://localhost:12390/"+ch_name+ch_id;

    function createSocket(host) {

        if ('WebSocket' in window)
            return new WebSocket(host);
        else if ('MozWebSocket' in window)
            return new MozWebSocket(host);

        throw new Error("No web socket support in browser!");
    }

    function init() {
        //var host = "ws://localhost:12390/General1";
        
        try {
            socket = createSocket(host);
            console.log('WebSocket - status ' + socket.readyState);
            socket.onopen = function(msg) {
                console.log("Welcome - status " + this.readyState);
            };
            socket.onmessage = function(msg) {
                
                data = JSON.parse(Decrypt(document.getElementsByClassName('channelActive')[0].dataset.key, msg.data));
                console.log(document.getElementsByClassName('channelActive')[0].dataset.key);
                log(data, false);
            };
            socket.onclose = function(msg) {
                console.log("Disconnected - status " + this.readyState);
            };
        }
        catch (ex) {
            console.log(ex);
        }
        document.getElementById("MsgBox").focus();
    }


    function send(msg) {

        try {
            let key = document.getElementById('public').innerHTML;
            enc = encodeURIComponent(Encrypt(key, msg));
            ide = encodeURIComponent(Encrypt(key, 'Hi it is Me'));
            response = {
                'user_id': user_id,
                'message': enc,
                'identity': ide,
                'mime': 'text',
                'channel_id': document.getElementsByClassName('channelActive')[0].dataset.id
            };
            socket.send(JSON.stringify(response));
        } catch (ex) {
            console.log(ex);
        }
    }


    function quit() {
        log("Goodbye!");
        socket.close();
        socket = null;
    }


    function log(msg, encoded) 
    {
        if(encoded) {
        var key = document.getElementsByClassName('channelActive')[0].dataset.key;
        var decrypt = new JSEncrypt();
        decrypt.setPrivateKey(key);
        decoded =  msg;
        var uncrypted = decrypt.decrypt(decoded);
        }
        else {
            var uncrypted = msg.response;
        }
        console.log(msg.created);
        document.getElementById("chats").innerHTML += "<p>" +
        '<span class="fa fa-user"> '+
            msg.user_name+':'+
        '</span> '+
        '<span class="messageTxt">'+
          uncrypted + '&nbsp;<small class="smallText">'+msg.created +'</small>' +
        '</span>'+
         "</p>";
    }


    function onkey(event) {
        if (event.keyCode == 13) {
            send();
        }
    }


    function Encrypt(Key, Msg)
    {
         let encrypt = new JSEncrypt();
         encrypt.setPublicKey(Key);
         return encrypt.encrypt(Msg);
    }
    

  function Decrypt(Key, Msg)
  {
     let decrypt = new JSEncrypt();
     decrypt.setPrivateKey(Key);
     return decrypt.decrypt(Msg);
  }

  function ChangeChannel(id)
    {
        quit();
        ch_id = document.getElementById(id).dataset.id;
        ch_name = document.getElementById(id).getAttribute('name');
        host = "ws://localhost:12390/"+ch_name+ch_id;
        FreeChannel(ch_id);
        init();
        document.getElementsByClassName('channelActive')[0].classList.remove('channelActive');
        document.getElementById(id).classList.add('channelActive');
            
    }

    function FreeChannel(id)
    {
        Form = new FormData();
        Form.append('channel_id', id);
        Ajax.Method = 'POST';
        Ajax.Target = RefreshChannel;   
        Ajax.Parameters = Form;
        Ajax.Ajax(callBackChannel, onErrorChannel, onCloseChannel);
    }


    function callBackChannel(response) 
    {
    if(response.Errorno != 0) {
        alert(response.response);
    }
    else {
        document.getElementById('chats').innerHTML = "<h2><strong>Chat</strong></h2>"+
        "<hr/>";
        auth = document.getElementById('auth').dataset.id;
        for(i=0; i < response.response.length; i++)
        {
            if(response.response[i].user.id == auth) {
                document.getElementById('chats').innerHTML+= '<p> <span class="fa fa-user faUser"> '+
                response.response[i].user.name+':'+
                '</span> '+
            ' <span class="messageTxt">'+
            response.response[i].message+' &nbsp;'+
            '<small class="smallText">'+response.response[i].created_at+'</small>'+
        '</span></p>'
            }
            else {
                document.getElementById('chats').innerHTML+= '<p> <span class="fa fa-user fanotUser"> '+
                response.response[i].user.name+':'+
            '</span> '+
            ' <span class="messageTxt">'+
            response.response[i].message+' &nbsp;'+
            '<small class="smallText">'+response.response[i].created_at+'</small>'+
        '</span></p>'
            }
            
            
        }
    }
    }

    //@onErrorChannel use to support Ajax call for Channels
    function onErrorChannel(ready, status)
    {
        alert("Error Occured on Server!\nReady: " + ready + "\nStatus: " + status);

    }

    //@onCloseChannel use to support Ajax call for Channels
    function onCloseChannel()
    {
        
        return;
    }

    window.onload = function() {
        document.getElementById('MsgBox').addEventListener('focus', function () {
            document.getElementById('Go').style.borderColor='grey';
            document.getElementById('Go').style.color='grey';
        });

        document.getElementById('MsgBox').addEventListener('blur', function () {
            document.getElementById('Go').style.borderColor='#D0D0D0';
            document.getElementById('Go').style.color='#D0D0D0';
        });


        var socket;
        socket = init();
        document.getElementById('MsgBox').addEventListener('keypress', function(e) {
            if(e.code === 'Enter') {
                send(document.getElementById('MsgBox').value)
                document.getElementById('MsgBox').value = "";
            }
            else {
                return;
            }
        });

        


   };

   var Ajax = {
    Method: 'GET',
    Target: '',
    Parameters: null,
    Ajax: function(callBack, onError, onClose) {
        if (window.XMLHttpRequest) {
            connection = new XMLHttpRequest();
        }

        connection.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                response = this.responseText;
                console.log(response);
                response = JSON.parse(response);
                if(callBack != null) {
                    callBack(response);
                }
              
            }
            else if(this.readyState == 4 && this.status != 200) {
                if(onError != null) {
                    onError(this.readyState, this.status);
                }
            }

            if(onClose != null && this.readyState == 4) {
                onClose();
            }

            
        };
        connection.open(this.Method, this.Target, true);
        if(this.Method == "POST") {
            token  = document.getElementsByName('csrf-token')[0].content;
            connection.setRequestHeader("X-CSRF-Token", token);
        }
        connection.send(this.Parameters);
    }
};
