<?php

use Illuminate\Database\Seeder;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'ahmad',
            'email' => 'ai@hotmail.com',
            'password' => bcrypt('123456')
        ]);

        for($i = 0; $i < 50; ++$i)
        {
            DB::table('users')->insert([
                'name' => str_random(10),
                'email' => str_random(5).'@hotmail.com',
                'password' => bcrypt('123456')
            ]);
        }
    }
}
