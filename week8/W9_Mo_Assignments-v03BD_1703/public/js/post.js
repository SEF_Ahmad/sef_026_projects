window.onload = function () {

    //set listener to detect 'Enter' on comment text box, and send comment
    document.getElementById('addComment').addEventListener('keypress', function(e){
        if(e.code === 'Enter') {
            Form = new FormData();
            Form.append('post_id', post_id);
            Form.append('comment', document.getElementById('addComment').value);
            Ajax.Method = 'POST';
            Ajax.Target = commentPostURL;   
            Ajax.Parameters = Form;
            Ajax.Ajax(callBackComment, onErrorComment, onCloseComment);
        }
        else {
            return;
        }
    });

    //set listener to focus on comment text box
    document.getElementById('commentFocus').addEventListener('click', function(){
        document.getElementById("addComment").focus();
    });


    //set listener to handle like button
    document.getElementById('postLike').addEventListener('click', function(){
        Form = new FormData();
        Form.append('post_id', post_id);
        Ajax.Method = 'POST';
        if(document.getElementById('postLike').dataset.like == 1) {
            Ajax.Target = unlikePostURL;
        }
        else {
            Ajax.Target = likePostURL;
        }
        Ajax.Parameters = Form;
        Ajax.Ajax(callBackLike, onErrorLike, onCloseLike);
    });


    //set listener to handle follow button
    document.getElementById('btnFollow').addEventListener('click', function(){
        Form = new FormData();
        user_id = document.getElementById('btnFollow').dataset.id;
        Form.append('user_id', user_id);
        Ajax.Method = 'POST';
        if(document.getElementById('btnFollow').dataset.follow == 1) {
            Ajax.Target = followURL;
        }
        else {
            Ajax.Target = unfollowURL;
        }
        Ajax.Parameters = Form;
        Ajax.Ajax(callBackLike, onErrorLike, onCloseLike);
    });
};



function callBackComment(response) 
{
   if(response.Errorno != 0) {
       alert(response.response);
   }
   else {
       window.location = postURL + '\/' + response.response;
   }
}

//@onErrorComment use to support Ajax call for comments
function onErrorComment(ready, status)
{
    alert("Error Occured on Server!\nReady: " + ready + "\nStatus: " + status);

}

//@onCloseCommentl use to support Ajax call for comments
function onCloseComment()
{
    
    return;
}



function callBackLike(response) 
{
   if(response.Errorno != 0) {
       alert(response.response);
   }
   else {
       if(response.response == '1') {
           document.getElementById('postLike').classList.add('liked');
           document.getElementById('postLike').dataset.like = 1;
       }
       else {
        document.getElementById('postLike').classList.remove('liked');
        document.getElementById('postLike').dataset.like = 0;
       }
   }
}

//@onErrorlike use to support Ajax call for comments
function onErrorLike(ready, status)
{
    alert("Error Occured on Server!\nReady: " + ready + "\nStatus: " + status);

}

//@onCloselike use to support Ajax call for likes
function onCloseLike()
{
    return;
}