<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class LikeController extends Controller
{
    function __Construct()
    {
        $this->middleware('auth');
    }


    public function isLike($post_id) 
    {
        return $like = \App\Like::where([
            ['post_id', '=',  $post_id],
            ['owner_id', '=',  Auth::id()]
        ])
        ->count();
    }
    
    public function Like(Request $request)
    {
        if($this->isLike($request->get('post_id')) > 0) {
            return false;
        }
        $like = new \App\Like;
        $like->post_id = $request->get('post_id');
        $like->owner_id = Auth::id();
        $like->like = true;
        $like->save();
        return $like->id;

    }


    public function unLike(Request $request)
    {
        if($this->isLike($request->get('post_id')) <= 0) {
            return false;
        }
        $like = \App\Like::where('post_id', $request->get('post_id'))
        ->where('owner_id', Auth::id())->first();
        \App\Like::Destroy($like->id);
        return true;
    }
}
