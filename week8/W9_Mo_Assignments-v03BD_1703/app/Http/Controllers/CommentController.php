<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    //
    function __Construct()
    {
        $this->middleware('auth');
    }

    public function Create(Request $request)
    {
        
        $Errorno = 0;
        $Error = "";
        $Response = "";
        if(!Auth::Check()) {
            $Response = "Un Signed User";
            $Errorno = 100;
        }
        else {
            $validator = Validator::make($request->all(), 
                    [
                        'comment' => 'max:1024|min:1',
                        'post_id' => 'required',
                    ]
                );  
            
            if ($validator->fails()) {
                $Errorno = 105;
                $Response = "Post Id required, and at leat 1 character comment";
            }
            else {
                $Response = $this->Store($request);
            }

            
        }

        return response()->json(array(
            'response'=> $Response,
            'Errorno' => $Errorno,
            'Error' => $Error
        ), 200);
    }



    public function Store(Request $request)
    {
        $comment = new \App\Comment;
        $comment->comment = $request->get('comment');
        $comment->post_id = $request->get('post_id');
        $comment->user_id = Auth::id();
        $comment->save();
        return $comment->post_id;
    }
}
