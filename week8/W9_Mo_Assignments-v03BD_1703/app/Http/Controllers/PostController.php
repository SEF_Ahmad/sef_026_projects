<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Validator;

class PostController extends Controller
{

    function __Construct()
    {
        $this->middleware('auth');
    }



    public function ShowAll($id)
    {
        $user = \App\User::where('id', $id)->first();
        if($id == Auth::id()) {
            $follow = -1;
        }
        else {
           $follow = \App\Follow::where('following_id', Auth::id())
                        ->where('followed_id', $user->id)->first();
        }
        
        return view('posts',[
            'Posts' => \App\Post::where('owner_id', $id)->get(),
            'User' => $user,
            'Follow' => $follow
            ]);
    }


    public function Show($id)
    {
        $Post = \App\Post::findOrFail($id);
        $ImgController = new ImageController;
        $Img = $ImgController->getPath($Post->image_id);
        $User = \App\User::findOrFail($Post->owner_id);
        $Comments = \App\Comment::where('post_id', $id)->get();
        $like = new LikeController();

        return view('postshow', [
            'Post' => $Post,
            'Img' => $Img,
            'User' => $User,
            'Comments' => $Comments,
            'isLike' => $like->isLike($id)
        ]);
    }



    public function Create(Request $request)
    {
        $Errorno = 0;
        $Error = "";
        $Response = "";
        if(!Auth::Check()) {
            $Response = "Un Signed User";
            $Errorno = 100;
        }
        else {
            $validator = Validator::make($request->all(), 
                    [
                        'caption' => 'max:1024',
                        'picture' => 'required|mimetypes:image/jpeg,image/jpg,image/png',
                    ]
                );  
            
            if ($validator->fails()) {
                $Errorno = 105;
                $Response = "Image of type jpeg or png is required";
            }
            else {
                $Img = new ImageController();
                $ImgId = $Img->Create($request);
                $Response = $this->Store($request, $ImgId);
            }
        }
        sleep(1); //Give time for spinner to work :D 
        return response()->json(array(
            'response'=> $Response,
            'Errorno' => $Errorno,
            'Error' => $Error
        ), 200);
    }



    public function Like(Request $request)
    {
        $Errorno = 0;
        $Error = "";
        $Response = "";
        if(!Auth::Check()) {
            $Response = "Un Signed User";
            $Errorno = 100;
        }
        else {
            $validator = Validator::make($request->all(), 
                    [
                        'post_id' => 'required',
                    ]
                );  
            
            if ($validator->fails()) {
                $Errorno = 105;
                $Response = "Something went wrong";
            }
            else {
                $like = new LikeController();
                $result = $like->Like($request);
                if($result == false) {
                    $Errorno = 144;
                    $Response = "Can't like liked";
                }
                else {
                    $Response = '1';
                }
            }
        }

        return response()->json(array(
            'response'=> $Response,
            'Errorno' => $Errorno,
            'Error' => $Error
            ), 200);

    }



    public function UnLike(Request $request)
    {
        $Errorno = 0;
        $Error = "";
        $Response = "";
        if(!Auth::Check()) {
            $Response = "Un Signed User";
            $Errorno = 100;
        }
        else {
            $validator = Validator::make($request->all(), 
                    [
                        'post_id' => 'required',
                    ]
                );  
            
            if ($validator->fails()) {
                $Errorno = 105;
                $Response = "Something went wrong";
            }
            else {
                $like = new LikeController();
                $result = $like->unLike($request);
                if($result == false) {
                    $Errorno = 144;
                    $Response = "Can't unlike unliked";
                }
                else {
                    $Response = '0';
                }
            }
        }

        return response()->json(array(
            'response'=> $Response,
            'Errorno' => $Errorno,
            'Error' => $Error
            ), 200);

    }



    public function Store(Request $request, $ImgId)
    {
        $post = new \App\Post;
        if(!$request->has('caption') || $request->get('caption') == '') {
            $post->description = "";
        }
        else {
            $post->description = $request->get('caption');
        }
        $post->image_id = $ImgId;
        $post->owner_id = Auth::id();
        $post->save();
        return $post->id;
    }

}
