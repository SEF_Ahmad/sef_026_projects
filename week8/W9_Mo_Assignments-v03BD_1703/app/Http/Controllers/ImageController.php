<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function Create(Request $request)
    {
        $Img = new \App\Image;
        $Img->name = $request->file('picture')->getClientOriginalName();
        $Img->owner_id = Auth::id();
        $Img->mime = $request->file('picture')->getClientMimeType();
        $Img->save();
        Storage::putFileAs('public/', $request->file('picture'), $Img->id);
        return $Img->id;  
    }


    public function getPath($id)
    {
        $Img = \App\Image::findOrFail($id);
        $storagePath  = url('storage/'.$id);
        return $storagePath;
    }

}
