<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    public $timestamps = true;

    public function Posts()
    {
        return $this->hasOne('App\Post');
    }

    public function Users()
    {
        return $this->hasOne('User');
    }
    
}
