<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    public $timestamps = true;

    public function Images()
    {
        return $this->hasOne('App\Image');
    }


    public function Comments()
    {
        return $this->hasMany('App\Comment');
    }
}
