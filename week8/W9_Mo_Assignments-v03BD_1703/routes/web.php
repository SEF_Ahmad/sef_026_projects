<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Route::get('/post/create', 'PostController@Create');

Route::post('/post/create', 'PostController@Create');

Route::post('/test', 'PostController@Create');

Route::get('/post/{id}', 'PostController@Show');

Route::post('/comment', 'CommentController@Create');

Route::post('/post/like', 'PostController@Like');

Route::post('/post/unlike', 'PostController@UnLike');

Route::get('/explore', 'HomeController@Explore');

Route::get('/posts/{id}', 'PostController@ShowAll');

Route::post('/user/follow', 'UserController@Follow');

Route::post('/user/unfollow', 'UserController@unFollow');