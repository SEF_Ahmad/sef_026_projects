@extends('layouts/app')


@section('content')

<div class="row">
    <div class="col col-md-6 offset-md-3">
        <h2 class="exploreHeader">Find Friends</h2>
    </div>
</div>

<div class="row">
@foreach($Users as $user)

<div class="col col-md-1"><p> </p></div>
<div class="col col-md-3">
    <a href="{{url("/posts/$user->id")}}"> 
        <div class="row">
            <span class="fa fa-user fa-3x col-md-12 userPic"></span>
        </div>
        <div class="row">
            <span class="userName col-md-12">{{$user->name}} </span> 
        </div>
    </a>
</div>

@endforeach
</div>



@endsection