@extends('layouts/app')


@section('content')

<div class="postContainer" data-id="{{$Post->id}}" id="postContainer"> 
    <div class="col col-md-5 offset-md-1">
        <img src="{{url($Img)}}" class=""/>
    </div>

    <div class="col col-md-5 postComments">
        <span class="fa fa-user fa-2x"> {{$User->name}}</span>
        <p class="postText">
            {{$Post->description}}
        </p>
        <hr/> 

        
        @foreach($Comments as $comment)
            <div class="row">
                <div class="col col-md-10 offset-md-1">
                    <p>
                        <span class="fa fa-user"> {{\App\User::where('id', $comment->user_id)->first()->name}}</span>
                        {{$comment->comment}}
                    </p>
                </div>
            </div> 
        @endforeach
           
        <hr/>
        <div class="row commentFooter" >
            <div class="col col-md-10 offset-md-1">

            @if($isLike == 1)
                <span id="postLike" data-like="{{$isLike}}" class="far fa-heart fa-2x liked"></span>
            @else
                <span id="postLike" data-like="{{$isLike}}" class="far fa-heart fa-2x"></span>
            @endif

                <span> &nbsp;&nbsp; </span>
                <span id="commentFocus" class="far fa-comment fa-2x"></span> 
            </div>
        </div> 

        <br/>
        <div class="row">
            <div class="col col-md-10 offset-md-1">
                <input id="addComment" name="addComment" placeholder="Add Comment" type="text" class="form-control instaBox "/>
            </div>
        </div> 
    </div>
    
</div>


<script type="text/javascript">
    var post_id = document.getElementById('postContainer').dataset.id;
    var commentPostURL = "{{ URL("/comment") }}";
    var likePostURL = "{{ URL("/post/like") }}";
    var unlikePostURL = "{{ URL("/post/unlike") }}";
</script>



@endsection