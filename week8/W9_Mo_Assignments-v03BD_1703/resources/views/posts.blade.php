@extends('layouts/app')


@section('content')


<div class="row exploreHeader">
    <div class="col col-md-6 offset-md-3">
     <h2 class=""> <span class="fa fa-user"></span> {{$User->name}}&apos;s Profile</h2>
    </div>

    @if($Follow != -1)
        <div class="col col-md-2">
            <button type="button" id="btnFollow" data-id="{{$User->id}}" data-follow="{{$Follow}}" class="btn btn-primary followButton"> Follow </button>
        </div>
    @endif
</div>


<div class="row">
    @foreach($Posts as $post)

    <div class="col col-md-1"><p> </p></div>
    <div class="col col-md-3">
        <a href="{{url("/post/$post->id")}}"> 
            <div class="row">
                <img src="{{url("/storage/$post->image_id")}}" class="fluid-image col-md-12"/>
            </div>
            <div class="row">
                <p class="postDate col-md-12"> {{$post->created_at}} </p>
            </div>
        </a>
    </div>

    @endforeach
</div>

<script type="text/javascript">
    var followURL = "{{url('/user/follow')}}";
    var unfollowURL = "{{url('/user/unfollow')}}";

</script>

@endsection