var response;


function CallServer(value) {
    if (window.XMLHttpRequest) {
        connection = new XMLHttpRequest();
    }
    
    connection.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            response = this.responseText;
            response = JSON.parse(response);
            if(response.Errorno != 0) {
                document.getElementById("summaryText").innerHTML += 
                '<div class="Error">' + '<strong> Error(' + response.Errorno
                 + '): </strong>' + response.Error + '</div>'
            }
            else {
                FetchText(response.Response);
            }
            
            document.getElementById("wait").style.visibility = "hidden";
        }
    };
    connection.open("GET", "Api.php?address=" + value, true);
    connection.send();
    return response;
}


function FetchText(HTML)
{
    var VirtualDom = document.createElement("Virtual");
    VirtualDom.innerHTML = HTML;
    //console.log("html:" + HTML);
    let Main = VirtualDom.getElementsByClassName("section-content");
    let Text = "";
    for(let count =0; count < Main.length; ++count)
    {
        Text += Main[count].innerHTML.replace(/<(.|\n)*?>/g, '') + " ";
    }
    Text = Text.replace(/\&nbsp\;/g, ' ');
    Text = Text.replace(/\.|\?|\!/g, ". ");
    let Titles = VirtualDom.getElementsByClassName("graf graf--h3");
    let Title="";
    for(let count =0; count < Titles.length; ++count)
    {
        Title += Titles[count].innerHTML + " ";
    }
    document.getElementById("summaryTitle").innerHTML = Title;
    CallAylien(Title, Text);
}


function CallAylien(title, text)
{
    connection = new XMLHttpRequest();
    let URL = "Summary.php";
    let params = "title="+title+"&text="+text;
    connection.open("POST", URL, true);
    connection.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    var summary;
    connection.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            summary = this.responseText;
            //alert(summary);
            summary = JSON.parse(summary);
            
            for(let i=0; i < summary.sentences.length; ++i)
            {
                document.getElementById("summaryText").innerHTML += 
                "<strong>"+ (i+1) + "</strong>" +
                "- " +  summary.sentences[i]+ "<br/><br/>";
            }

        }
    };
    
    connection.send(params);
}



document.getElementById("Go").addEventListener("click", function(){

    document.getElementById("summaryTitle").innerHTML = "";
    document.getElementById("summaryText").innerHTML = "";
    let address = document.getElementById("addressBar").value;
    document.getElementById("wait").style.visibility = "visible";
    document.getElementById("wait").style.height = "168px";
    response = CallServer(address);
}); 
