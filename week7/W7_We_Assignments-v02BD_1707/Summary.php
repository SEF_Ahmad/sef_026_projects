<?php 


$ch = curl_init();
$sentencesNumber = 4;
$title = urlencode($_POST["title"]);
$text = urlencode($_POST["text"]);
$wordCount = str_word_count($_POST["text"]);
if($wordCount <= 300) {
    $sentencesNumber = 3;
}
elseif($wordCount <= 1000) {
    $sentencesNumber = 5;
}
elseif($wordCount <= 2000) {
    $sentencesNumber = 7;
}
else {
    $sentencesNumber = 10;
}
curl_setopt($ch, CURLOPT_URL, "https://api.aylien.com/api/v1/summarize");
curl_setopt($ch, CURLOPT_HTTPHEADER, array (
        'X-AYLIEN-TextAPI-Application-Key: b02ea527027ecda8927779f7436341e3',
        'X-AYLIEN-TextAPI-Application-ID: 753b8606'
    ));
//error_log(var_dump(array("text=$text","title=$title")));
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, array(
        "text" =>$text,
        "title" =>$title,
        "sentences_number" => $sentencesNumber
    ));

curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$Response = curl_exec($ch);
curl_close($ch);

echo urldecode($Response);