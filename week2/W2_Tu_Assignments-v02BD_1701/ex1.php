#!/usr/bin/php
<?php
/**
 * @Author: Ahmad Issa 
 * @CLI options: 
 *   -a [optional] determines the author name
 *   -e [optional] set Email
 *   -d [optional] set date by format D-M-Y
 *   -t [optional] set time
 *   -s [optional] set timestamp
 *   -m [optional] search by message
*/

//Regex definition
define ("KEY_COMMIT", "/^commit\b/");
define ("KEY_AUTHOR", "/^Author\b/");
define ("KEY_EMAIL", "/\<.*@.*\..*\>/");
define ("KEY_DATE", "/^Date\b/");
define ("KEY_MERGE", "/^Merge\b/");
define ("SEARCH_ALL", "^.*$");

//Error definition
define ("ERR_NO_AUTHOR", 0);
define ("ERR_EMAIL", 1);
define ("ERR_TIME", 2);
define ("ERR_DATE", 3);
define ("ERR_STAMP", 4);

//Search options
$Author     = SEARCH_ALL;
$Email      = SEARCH_ALL;
$Date       = SEARCH_ALL;
$Time       = SEARCH_ALL;
$Timestamp  = SEARCH_ALL;
$Message    = SEARCH_ALL;


class Commit
{
    public $Author;
    public $Email;
    public $Date;
    public $Time;
    public $Timestamp;
    public $DTRaw;
    public $Message;
    public $SHA1;
    
    function __Construct()
    {
        $this->Author = "";
        $this->Email = "";
        $this->Date = "";
        $this->Time = "";
        $this->Timestamp = "";
        $this->DTObject = NULL;
        $this->Message = "";
        $this->SHA1 = "";
    }  
    
    public function Search($A, $E, $D, $T, $S, $M)
    {
        $AC = preg_match("/$A/", $this->Author);
        $EC = preg_match("/$E/", $this->Email);
        $DC = preg_match("/$D/", $this->Date);
        $TC = preg_match("/$T/", $this->Time);
        $SC = preg_match("/$S/", $this->Timestamp);
        $MC = preg_match("/$M/", $this->Message);
        if($AC && $EC && $DC && $TC && $SC && $MC) {
            return $this;
        }
        else {
            return NULL;
        }
           

    }
}



function KeyMatch($Key, $Line)
{
    $output = array();
    $Offset = 0;
    $NMatch = preg_match($Key, $Line, $output, PREG_OFFSET_CAPTURE);
    if($NMatch > 0) {
        $Offset  = $output[0][1];
        return $Offset;
    }
    else {
        return -1;
    }
}


function Parse($Input)
{
    $Commits = array();
    $c = new Commit;
    $LengthAuthor = strlen('Author:');
    foreach($Input as $line) {
        if(KeyMatch(KEY_COMMIT,$line) >= 0){
            if($c->SHA1 != ""){
                array_push($Commits, $c);	
                $c = new Commit;
            }
            $c->SHA1 = trim(substr($line, strlen('commit')));
        }
        else if(KeyMatch(KEY_AUTHOR, $line) >= 0) {
            $E = KeyMatch(KEY_EMAIL, $line);
            if($E >= 0) {
                $c->Author = trim(substr($line, $LengthAuthor, $E - $LengthAuthor));
                $c->Email = trim(substr($line, $E));
                $c->Email = substr($c->Email, 1, strlen($c->Email) -2); //remove <>
            }
            else {
                $c->Author= trim(substr($line, $LengthAuthor));
            }
            
        }
        else if(KeyMatch(KEY_DATE, $line) >= 0) {
            $Date = trim(substr($line, strlen('Date:')));
            $DTObj = date_create_from_format ('D M j H:i:s Y P' , $Date);
            $c->Date = $DTObj->format('j-m-Y');
            $c->Time = $DTObj->format('H:i');
            $c->Timestamp = $DTObj->getTimestamp();
            $c->DTRaw = $Date;
        }
        else if(KeyMatch(KEY_MERGE, $line) >= 0 ) {
            //Do nothing.
        }
        else {		
            $c->Message .= $line;
        }
    }
    return $Commits;
}



function filterOptions($options)
{
    global $Author;
    global $Email;
    global $Date;
    global $Time;
    global $Timestamp;
    global $Message;

    if(array_key_exists("a", $options)) {
        $Author = $options["a"];
        if($Author == "") {
            RaiseError(ERR_NO_AUTHOR);
        }
     }
     
     if(array_key_exists("e", $options)) {
         $Email = $options["e"];
         if(!filter_var($Email, FILTER_VALIDATE_EMAIL)) {
             RaiseError(ERR_EMAIL);
         }
      }
     
     if(array_key_exists("t", $options)) {
         $Time = $options["t"];
         if(!preg_match('/[0-9]{1,2}\:[0-9]{1,2}/', $Time)) {
             RaiseError(ERR_TIME);
         }
      }
     
      if(array_key_exists("d", $options)) {
         $Date = $options["d"];
         if(!preg_match('/[0-9]{1,2}\-[0-9]{2}\-[0-9]{4}/', $Date)) {
             RaiseError(ERR_DATE);
         }
      }
     
      if(array_key_exists("s", $options)) {
         $Timestamp = $options["s"];
         if(!preg_match('/[0-9]+/', $Timestamp)) {
             RaiseError(ERR_STAMP);
         }
      }
     
      if(array_key_exists("m", $options)) {
         $Message = $options["m"];
      }
}


/*
 * This function builds the first sentence in results, like: 
   Search results by Author ("Bassem") - Total: 4
*/
function resultStringBuild()
{
    global $Author;
    global $Email;
    global $Date;
    global $Time;
    global $Timestamp;
    global $Message;
    $ResultOut = "";
    $OptionsIn = 0;

    if($Author != SEARCH_ALL) {
        $ResultOut .= "Author ('$Author')";
        $OptionsIn++;
    }
    if($Email != SEARCH_ALL) {
        if($OptionsIn > 0) {
            $ResultOut .=", ";
        }
        $ResultOut .= "Email ('$Email')";
        $OptionsIn++;
    }

    if($Date != SEARCH_ALL) {
        if($OptionsIn > 0) {
            $ResultOut .=", ";
        }
        $OptionsIn++;
        $ResultOut .= "Date ('$Date')";
    }

    if($Time != SEARCH_ALL) {
        if($OptionsIn > 0) {
            $ResultOut .=", ";
        }
        $OptionsIn++;
        $ResultOut .= "Time ('$Time')";
    }

    if($Timestamp != SEARCH_ALL) {
        if($OptionsIn > 0) {
            $ResultOut .=", ";
        }
        $OptionsIn++;
        $ResultOut .= "Timestamp ('$Timestamp')";
    }

    if($Message != SEARCH_ALL) {
        if($OptionsIn > 0) {
            $ResultOut .=", ";
        }    
        $OptionsIn++;
        $ResultOut .= "Message ('$Message')";
    }
    if($OptionsIn == 0) {
        $ResultOut = "Everything";
    }
    return $ResultOut;
}


function RaiseError($Errno)
{
    switch ($Errno)
    {
        case ERR_NO_AUTHOR:
             exit("Input for author name not allowed\n");
        case ERR_EMAIL:
             exit("Input for email is not validated\n");
        case ERR_TIME:
             exit("Input for time should be like H:mm\n");
        case ERR_DATE:
             exit("Input for date should be like D-M-Y\n");
        case ERR_STAMP:
             exit("Input for timestamp should contain only numeric\n");
        
        default:
             exit("Undefined error occured\n");

    }
}


//Entry point

$logs = array();
exec("git log",$logs);

//Process cli options
$cli_options = "a:e:t:d:s:m:";
$options = getopt($cli_options);
filterOptions($options);

//Do the search
$Commits = Parse($logs);
$Results = array();
$i = 0;
foreach($Commits as $commit)
{
    $Result = $commit->search($Author, $Email, 
                              $Date, $Time, 
                              $Timestamp, $Message);
    if($Result != NULL) {
        $i++;
        array_push($Results, $Result);
    }
}

//Write the results
echo "Search results by: ".resultStringBuild()." - Total: $i\n\n";
$i = 0;
foreach($Results as $r)
{
    $i++;
    echo "\t$i:: $r->SHA1 - $r->Author - $r->DTRaw \n\n\t $r->Message\n\n\n";
}
