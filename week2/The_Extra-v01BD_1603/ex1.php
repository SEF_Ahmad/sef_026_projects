#!/usr/bin/php
<?php
/**
 * @Author: Ahmad Issa
 * @Credit: Important parts of the solution to the puzzle
 * are provided by Ali Atieh.
 * @Solution: The N'th person say Black if Black is even, white if Black is odd
 * @CLI options: 
 *   -l [optional] determines the solution technique, if it's deterministic
 *    or probabilistic. Deterministic by default.
 *   -f [optional] provide file name, however it has default value.
 *   -n [optional] provide number of people to expect when reading file
 *    by default it count file entries.
*/

define("WHITE", "W");
define("BLACK", "B");
define("ERROR", "e");
define("DETERMINISTIC", "d");
define("PROBABILISTIC", "p");

$cli_options = "l:f:n:h::";

//options default values
$N = 0;
$Technique = DETERMINISTIC;
$FileName = "data.txt";

//Person is a class that mimic the individual person in the game
class Person
{
  private $Color;
  public $Number;
  private $ExpectedColor;

  public function __construct($number, $color)
  {
      $this->Color = $color;
      $this->Number = $number;
      $this->ExpectedColor = ERROR;
  }

  public function whisperColor($Asker)
  {
      if($Asker->Number > $this->Number) {
          return $this->Color;
      }
      else {
          return ERROR; //by definition those in front can't see my color
      }
  }

  public function getColor()
  {
      return $this->Color; //for outside use only
  }

  public function shoutColor()
  {
      return $this->ExpectedColor;
  }

  public function expectColor($others, $N)
  {
      $WhiteNumber = 0;
      $BlackNumber = 0;
      if($this->Number == $N){
          foreach($others as $o)
          {
              $wc = $o->whisperColor($this);
              if($wc == WHITE){
                  $WhiteNumber++;
              }
              elseif($wc == BLACK){
                  $BlackNumber++;
              }
          }
          if(($BlackNumber == 0 && $this->color != BLACK) || 
             ($WhiteNumber == 0 && $this->colro != WHITE)){
              RaiseError(0);
          }
          elseif($BlackNumber == $WhiteNumber || $BlackNumber % 2 == 0){
              $this->ExpectedColor = BLACK;
          }
          else{
              $this->ExpectedColor = WHITE;
          }
          return;
      }

      else{
          $NColor = $others[$N - 1]->shoutColor();
          foreach($others as $o)
          {
              $wc = $o->whisperColor($this);
              if($wc == WHITE){
                  $WhiteNumber++;
              }
              elseif($wc == BLACK){
                  $BlackNumber++;
              }
              else{
                  if($o->Number == $N){
                      continue;   //We can't count on what N person said about himself
                  }
                  $wc = $o->shoutColor();
                  if($wc == WHITE){
                      $WhiteNumber++;
                   }
                  elseif($wc == BLACK){
                          $BlackNumber++;
                   }
              }
          }
          if($NColor == BLACK){
              if($BlackNumber % 2 == 0){
                  $this->ExpectedColor = WHITE;
              }
              else{
                  $this->ExpectedColor = BLACK;
              }
          }
          else{
             if($BlackNumber % 2 == 0){
                 $this->ExpectedColor = BLACK;
             }
             else{
                 $this->ExpectedColor = WHITE;
             }
          }
      }
      
  }

}

//Entry point
$options = getopt($cli_options);
if(array_key_exists("f", $options)){
    $FileName = $options["f"];
}

if(array_key_exists("l", $options)){
    if($options["l"] == "det"){
        $Technique = DETERMINISTIC;
    }
    elseif($options["l"] == "pro"){
        $Technique = PROBABILISTIC; 
    }
    else{
        RaiseError(2);
    }
}

if(array_key_exists("h", $options)){
    exit("This program run the solution for the hat problem."
         ."(see: https://en.wikipedia.org/wiki/Hat_puzzle#Ten-Hat_Variant)\n"
        ."[options]:\n* -f to set path to data file\n* -l to set solution"
        ." technique\n* -h for help\n");
}

//Check if given or default file exists
if(!file_exists($FileName)){
    echo $options["f"]."\t";
    RaiseError(1);
}
$file = fopen($FileName, "r");
if(!$file){
    RaiseError(3);
}
$People = array();
while(($line = fgets($file)) != false)
{
    $line = trim($line);
    $N++;
    if(strcasecmp($line,"White") == 0 || strcasecmp($line, "W") == 0){
        array_push($People, new Person($N, WHITE));
    }
    elseif(strcasecmp($line,"Black") == 0 || strcasecmp($line, "B") == 0){
        array_push($People, new Person($N, BLACK));
    }
    else{
        RaiseError(4);
    }
}

//Now let them expect the hat color
for($i=$N;$i>0;--$i)
{
    $People[$i-1]->expectColor($People, $N);
}

//Write the results
$MissMatch = 0;
echo str_repeat("_",60)."\n\n\t\tNumber\tColor\tGuess\n\n";

for($i=$N;$i>0;--$i)
{
    if($People[$i - 1]->getColor() != $People[$i-1]->shoutColor()){
        $MissMatch++;
    }
    echo "\t\t  ".$People[$i-1]->Number."\t  ".$People[$i-1]->getColor()."\t  "
         .$People[$i-1]->shoutColor()."\n";
}

echo str_repeat("_",60)."\n\n";

if($MissMatch > 1 ){
    echo "The prisoners failed the test, I will kill them now.\n\n";
}
else{
    echo "congratulations! $MissMatch miss guess happened, you will live\n\n";
}


//---------------------------End of logic---------------------

function RaiseError($Errno)
{
    switch ($Errno)
    {
        case 0:
             exit("The game is being manipulated, both colors should be used"
                 . " at least once. Run away\n");
        case 1:
             exit("File provided does not exist\n");
        case 2:
             exit("You should provide proper technique command -l det/pro'n");
        case 3:
             exit("Can not open file, check your permissions\n");
        case 4:
             exit("Data provided is corrupted, expect White/Black or W/B\n");
        
        default:
             exit("Undefined error occured\n");

    }
}


