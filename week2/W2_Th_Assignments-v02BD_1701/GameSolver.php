<?php
/**
 * @Author: Ahmad Issa 
 * Reverse Polish notation like 4 5 -
 * 6+(30*4)+(120*12)+(360*24)+(720*24*4)+(720*24*16) = 355806
*/

define ("ADD", "+");
define ("MUL", "*");
define ("SUB", "-");
define ("DIV", "/");

class GameSolver
{
    private $Gen;
    public $Target;
    public $Chosen;
    public $Solutions;
    public $AllCombinations;
    public $Operators;
    public $Expressions;
    public $OpList;
    public $combination;
    public $S;
    public $IsExact;

    function __Construct($Generator)
    {
        $this->Gen = $Generator;
        $this->Target = $Generator->Target;
        $this->Chosen = $Generator->Chosen;
        $this->Solutions = array();
        $this->AllCombinations = array();
        $this->Expressions = array();
        $this->OpList = array(ADD, MUL, SUB, DIV);
        $this->combination = array();
        $this->Operators = array();
    }

    public function Refresh()
    {
        $this->AllCombinations = array();
        $this->Expressions = array();
        $this->combination = array();
        $this->Operators = array();
    }


    public function GenCombinations($list, $out,  $index, $c)
    {
        if($index < 1) {
            array_push($this->AllCombinations, $out);
            //echo "$out\n";
            $c++;
            return $c;
        }
        $o = $out;  
        for($i = 0; $i < count($list); ++$i)
        {
            $NextGeneration = $list;
            array_push($o, $list[$i]); //$o .= " " . $Ch[$i];
            array_splice($NextGeneration, $i, 1);
            $c = $this->GenCombinations($NextGeneration, $o, $index - 1, $c);
            $o = $out;
        }
        return $c;
    }



    public function GenArithmetic($depth)
    {
        if($depth > 0) {
            for($i=0;$i<count($this->OpList);$i++)
            {
                array_push($this->combination, $this->OpList[$i]);
                $this->GenArithmetic($depth-1);
                array_pop($this->combination);
            }
        }
        else {
            array_push($this->Operators, $this->combination);
        }
    }



    


    

    public function GenExp($ops, $nums)
    {
        $this->Expressions = array();
        $c = count($nums);
        $oc = count($ops);
        $Infix = array();
        //var_dump($ops);
        //exit(var_dump($Infix));
        if($c == 0 || $oc == 0) {
            return;
        }
        $BREAK = false;
        $Val = 0;

        
            $BREAK = false;
            $E = new SplStack();
            $Infix = array();
            $out = array();
            $num = $nums[0];
            //$tempo = $ops;
            //$tempn = $num;
            array_push($Infix, 1);
            for($j = 0; $j < $oc; $j++)
            {
               array_push($Infix, $ops[$j]);
               array_push($Infix, $j+2);
            }

            //@Build
           for($j=0; $j < count($Infix); ++$j)
           {
               $BREAK = false;
               if($Infix[$j] == MUL || $Infix[$j] == DIV) {
                  if($E->count() != 0) {
                   $top  = $E->pop();
                   }
                   else {
                       $E->push($Infix[$j]);
                       continue;
                   }
                   if($top == MUL || $top == DIV) {
                       array_push($out, $top);
                       $E->push($Infix[$j]);
                   }
                   else {
                       $E->push($top);
                       $E->push($Infix[$j]);
                   }
               }
               else if($Infix[$j] == ADD || $Infix[$j] == SUB) {
                if($E->count() != 0) {
                   $top  = $E->pop();
                   }
                   else {
                       $E->push($Infix[$j]);
                       continue;
                   }
                if($top == MUL || $top == DIV) {
                   array_push($out, $top);
                    while(1)
                    {
                        if($E->count() != 0) {
                            $top  = $E->pop();
                        }
                        else {
                            $E->push($Infix[$j]);
                            $BREAK = true;
                            break;
                        }
                        if($top == MUL || $top == DIV) {
                            array_push($out,$top);
                        }
                        else if($top == ADD || $top == SUB){
                            array_push($out,$top);
                            $E->push($Infix[$j]);
                            break;
                        }
                    }
                    if($BREAK) {
                       // break;
                    }
                }
                else {
                    array_push($out, $top);
                    $E->push($Infix[$j]);
                }
               }
               else {
                array_push($out, $Infix[$j]);
               }
           }
          while($E->count() > 0)
          {
              array_push($out, $E->pop());
          }
          array_push($this->Expressions, $out);

          //@Test Code uncomment to execute
          //var_dump($out);
          /*echo "Infix: ";
          for($y=0; $y < count($Infix); ++$y)
          {
              echo $Infix[$y]." ";
          }
          echo "\t Postfix: ";
          for($y=0; $y < count($out); ++$y)
          {
              echo $out[$y]." ";
          }
          echo "\n\n";*/
        
    }

    public function SolveExp($combs)
    {
        $Val = 0;
        $c = count($combs);
        $out = $this->Expressions[0];
        $oc = count($out);
        //var_dump($out);
        //exit("\n");
        for($i = 0; $i < $c; ++$i)
        {
            $solution = new SplStack();
            $exps = $combs[$i];
           // var_dump($exps);
            for($l = 0; $l < $oc; ++$l)
            {
                
                //var_dump($solution);
                if($out[$l] == MUL) {
                    //var_dump($out);
                    //echo $out[$l]."\n";
                    $op2 = $solution->pop();
                    $op1 = $solution->pop();
                    $solution->push($op1 * $op2);
                }
                else if($out[$l] == DIV) {
                    $op2 = $solution->pop();
                    $op1 = $solution->pop();
                    if($op1 % $op2 != 0) {
                        break;
                    }
                    $solution->push($op1 / $op2);
                }
                else if($out[$l] == ADD) {
                    $op2 = $solution->pop();
                    $op1 = $solution->pop();
                    $solution->push($op1 + $op2);
                }
                else if($out[$l] == SUB) {
                    $op2 = $solution->pop();
                    $op1 = $solution->pop();
                    if($op1 - $op2 < 0) {
                        break;
                    }
                    $solution->push($op1 - $op2);
                }
                else {
                    $solution->push($exps[$out[$l]-1]);
                }
            }
            
            //@Test
            if($solution->count() > 0) $Val = $solution->pop();
            //echo $Val."\n";
            if($Val == $this->Target) {
                for($p = 0; $p < count($out); $p++)
                {
                    if(is_numeric($out[$p])) {
                        $out[$p] = $exps[$out[$p]-1];
                    }
                }
                echo "\n\nExact Solution found (Postfix): ";
                for($p = 0; $p < count($out); $p++)
                {
                    echo $out[$p]." ";
                }
                $this->IsExact = true;
                echo ("\n");
                return;
                // array_push($Val);

            }
            else if($Val < $this->Target +10 && $Val > $this->Target -10) {
                if(!in_array($Val, $this->Solutions)) {
                    array_push($this->Solutions, $Val);
                  }
                }
        }
    }

    public function SolveE($operators, $nums)
    {
        $c = count($nums);
        $oc = count($operators);
        //echo "Th$c\n";
        if($c == 0 || $oc == 0) {
            return;
        }
        $BREAK = false;
        $Val = 0;
        for($i = 0; $i < $c; ++$i)
        {
            $ops = $operators;
           //$ops = $Exp[$i]->pop();
           //$num = $Exp[$i]->pop();
           $num = $nums[$i];
           //var_dump($num);
           $tempo = $ops;
           $tempn = $num;
           //var_dump($ops);
           //@MUltiply
           for($j = 0; $j < count($ops); $j++)
           {
               if($ops[$j] == MUL) {
                   $Val = $num[$j] * $num[$j + 1];
                   array_splice($num, $j, 2, $Val);
                   array_splice($ops, $j, 1);
               }
           }

           //@Divide
           for($j = 0; $j < count($ops); $j++)
           {
               if($ops[$j] == DIV) {
                if($num[$j] % $num[$j + 1] != 0) {
                    $BREAK = true;
                    break;
                }
                   $Val = $num[$j] / $num[$j + 1];
                   array_splice($num, $j, 2, $Val);
                   array_splice($ops, $j, 1);
                   
               }
           }
           if($BREAK) {
               $BREAK = false;
               continue;
           }
           //@Add
           for($j = 0; $j < count($ops); $j++)
           {
               if($ops[$j] == ADD) {
                   $Val = $num[$j] + $num[$j + 1];
                   array_splice($num, $j, 2, $Val);
                   array_splice($ops, $j, 1);
               }
           }

           //@Subtract
           for($j = 0; $j < count($ops); $j++)
           {
               if($ops[$j] == SUB) {
                   $Val = $num[$j] - $num[$j + 1];
                   if($Val < 0) {
                       $BREAK = true;
                       break;
                   }
                   array_splice($num, $j, 2, $Val);
                   array_splice($ops, $j, 1);
               }
           }
           if($BREAK) {
            $BREAK = false;
            continue;
        }
           //echo "$Val\n";
           //@Test
           if($Val == $this->Target || $Val < 0) {
               exit ("Exact Solution found: $Val ** $this->Target\n");
              // array_push($Val);

           }
           else if($Val < $this->Target +10 && $Val > $this->Target -10) {
            if(!in_array($Val, $this->Solutions)) {
                array_push($this->Solutions, $Val);
            }

           }
        }
    }


    public function SolveT($operators, $nums)
    {
        $c = count($nums);
        $oc = count($operators);
        $Infix = array();
        //echo "Th$c\n";
        if($c == 0 || $oc == 0) {
            return;
        }
        $BREAK = false;
        $Val = 0;
        for($i = 0; $i < $c; ++$i)
        {
            $E = new SplStack();
            $Infix = array();
            $out = array();
            $ops = $operators;
            //$ops = $Exp[$i]->pop();
            //$num = $Exp[$i]->pop();
            $num = $nums[$i];
            //var_dump($num);
            $tempo = $ops;
            $tempn = $num;
            array_push($Infix, $num[0]);
            for($j = 0; $j < $oc; $j++)
            {
               array_push($Infix, $ops[$j]);
               array_push($Infix, $num[$j+1]);
            }
            //echo "Inf\t".var_dump($Infix);
            //@Build
           for($j=0; $j < count($Infix); ++$j)
           {
               $BREAK = false;
               if($Infix[$j] == MUL || $Infix[$j] == DIV) {
                  if($E->count() != 0) {
                   $top  = $E->pop();
                   }
                   else {
                       $E->push($Infix[$j]);
                       continue;
                   }
                   if($top == MUL || $top == DIV) {
                       array_push($out,$top);
                       $E->push($Infix[$j]);
                   }
                   else {
                       $E->push($top);
                       $E->push($Infix[$j]);
                   }
               }
               else if($Infix[$j] == ADD || $Infix[$j] == SUB) {
                if($E->count() != 0) {
                   $top  = $E->pop();
                   }
                   else {
                       $E->push($Infix[$j]);
                       continue;
                   }
                if($top == MUL || $top == DIV) {
                   array_push($out, $top);
                    while(1)
                    {
                        if($E->count() != 0) {
                            $top  = $E->pop();
                        }
                        else {
                            $E->push($Infix[$j]);
                            $BREAK = true;
                            break;
                        }
                        if($top == MUL || $top == DIV) {
                            array_push($out,$top);
                        }
                        else if($top == ADD || $top == SUB){
                            array_push($out,$top);
                            $E->push($Infix[$j]);
                            break;
                        }
                    }
                    if($BREAK) {
                        //break;
                    }
                }
                else {
                    array_push($out, $top);
                    $E->push($Infix[$j]);
                }
               }
               else {
                array_push($out, $Infix[$j]);
               }
           }
          while($E->count())
          {
              array_push($out, $E->pop());
          }

          $solution = new SplStack();
          //echo count($out)."\n";
          //var_dump($out);
          for($l = 0; $l < count($out); ++$l)
          {
              //var_dump($solution);
              if($out[$l] == MUL) {
                  $op2 = $solution->pop();
                  $op1 = $solution->pop();
                  $solution->push($op1 * $op2);
              }
              else if($out[$l] == DIV) {
                  $op2 = $solution->pop();
                  $op1 = $solution->pop();
                  if($op1 % $op2 != 0) {
                      break;
                  }
                  $solution->push($op1 / $op2);
              }
              else if($out[$l] == ADD) {
                  $op2 = $solution->pop();
                  $op1 = $solution->pop();
                  $solution->push($op1 + $op2);
              }
              else if($out[$l] == SUB) {
                  $op2 = $solution->pop();
                  $op1 = $solution->pop();
                  if($op1 - $op2 < 0) {
                      break;
                  }
                  $solution->push($op1 - $op2);
              }
              else {
                $solution->push($out[$l]);
              }
          }
         
           //echo "$Val\n";
           //@Test
           if($solution->count() > 0) $Val = $solution->pop();
           //echo $Val."\n";
           if($Val == $this->Target) {
               exit ("Exact Solution found: $Val ** $this->Target\n");
              // array_push($Val);

           }
           else if($Val < $this->Target +10 && $Val > $this->Target -10) {
            if(!in_array($Val, $this->Solutions)) {
                array_push($this->Solutions, $Val);
            }

           }
        }
    }

    public function SolveR($operators, $nums)
    {
        $c = count($nums);
        $oc = count($operators);
        //echo "Th$c\n";
        if($c == 0 || $oc == 0) {
            return;
        }
        
        $Val = 0;
        for($i = 0; $i < $c; ++$i)
        {
            $BREAK = false;
            $ops = $operators;
           //$ops = $Exp[$i]->pop();
           //$num = $Exp[$i]->pop();
           $num = $nums[$i];
           //var_dump($num);
           $tempo = $ops;
           $tempn = $num;
           //var_dump($ops);
           //@MUltiply
           while(count($ops) != 0)
           {
               if($ops[0] == MUL) {
                   $Val = $num[0] * $num[1];
                   array_splice($num, 0, 2, $Val);
                   array_splice($ops, 0, 1);
               }
               //----------------------------------------
               else if($ops[0] == DIV) {
                if($num[0] % $num[1] != 0) {
                    $BREAK = true;
                       break;
                }
                   $Val = $num[0] / $num[1];
                   array_splice($num, 0, 2, $Val);
                   array_splice($ops, 0, 1);
                   
               }
              
               //----------------------------------------
              else if($ops[0] == ADD) {
                $Val = $num[0] + $num[1];
                array_splice($num, 0, 2, $Val);
                array_splice($ops, 0, 1);
            }
            //-------------------------------------------
           else if($ops[0] == SUB) {
                $Val = $num[0] - $num[1];
                if($Val < 0) {
                    $BREAK = true;
                    break;
                }
                array_splice($num, 0, 2, $Val);
                array_splice($ops, 0, 1);
            }
            
           }
           if($BREAK) {
               continue;
           }
           //echo "$Val\n";
           //@Test
           if($Val == $this->Target || $num[0] == $this->Target ||$Val < 0) {
               exit ("Exact Solution found: $Val ** $this->Target\n");
              // array_push($Val);

           }
           else if($Val < $this->Target +10 && $Val > $this->Target -10) {
            if(!in_array($Val, $this->Solutions)) {
                array_push($this->Solutions, $Val);
            }

           }
        }
    }
    
}