<?php
/**
 * @Author: Ahmad Issa
*/

require_once("GameGenerator.php");
require_once("GameSolver.php");
class GameOutput
{
    public $Gen;
    public $Sol;
    

    function __Construct($l1, $l2)
    {
        $this->Gen = new GameGenerator($l1, $l2, true);
        $n = $this->Gen->N;
        for($g = 1; $g <= $n; ++$g)
        {
            
            $this->Sol = new  GameSolver($this->Gen);
            $this->Gen = new GameGenerator($l1, $l2, false);
            echo "Game $g:\n\nChosen: {";
            for($f=0; $f < 6; ++$f)
            {
                echo $this->Sol->Chosen[$f];
                if($f < 5) {
                    echo ", ";
                }
            }
            echo "}\nTarget: "
                 .$this->Gen->Target."\n\n\n";
            
            for($i = 2; $i <= 6; ++$i)
            {
                $this->Sol->Refresh();
                $this->Sol->GenCombinations($this->Sol->Chosen, array(), $i, 0);
                if($i > 1) {
                    $this->Sol->GenArithmetic($i - 1);
                }
                else {
                    $this->Sol->GenArithmetic(1);
                }
                $oc = count($this->Sol->Operators);
                
                    
                for($U = 0; $U < $oc; ++$U)
                {
                    if($U % 10 == 0) echo "|";
                        
                    $this->Sol->GenExp($this->Sol->Operators[$U], 
                                        $this->Sol->AllCombinations);
                    $this->Sol->SolveExp($this->Sol->AllCombinations);
                    if($this->Sol->IsExact) {
                        break;
                    }
                    /* $this->Sol->SolveR($this->Sol->Operators[$U],
                                        $this->Sol->AllCombinations);
                                        */   
                    //$this->Sol->SolveT($this->Sol->Operators[$U],
                                        // $this->Sol->AllCombinations);
                }
                if($this->Sol->IsExact) {
                    break;
                }
                
            }
            if($this->Sol->IsExact) {
                $this->Sol->IsExact = false;
                echo "\n\n----------------------\n\n";
                continue;
            }
            echo "\n\nNear Solutions: \n";
            for($i = 0; $i < count($this->Sol->Solutions); $i++)
            {
                echo "- ".$this->Sol->Solutions[$i]."\n";
            }
            echo "\n\n----------------------\n\n";
            //$this->Gen = new GameGenerator($l1, $l2, false);
        
        } //end for g
    }
}