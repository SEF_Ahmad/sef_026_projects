<?php
/**
 * @Author: Ahmad Issa 
 * 
*/

//Error definitions
define ("ERR_N", 0);

class GameGenerator
{
    public $N;
    public $Target;
    public $Chosen; //array of 6 items

    function __Construct($L1, $L2, $Ask)
    {
        if($Ask) {
            $in = readline("How many games would you like me to play today? ");
            $in = trim($in);
            if(is_numeric($in) && $in > 0) {
                $this->N = (int)$in;
            }
            else {
                RaiseErrorGEN(ERR_N);
            }
    }

       $this->GenerateTarget();
       $this->Choose($L1, $L2);

    }

    private function GenerateTarget()
    {
        $this->Target =  random_int(101, 999);
    }

    private function Choose($List1, $List2)
    {
        $Choose_List1 = random_int(1, 4);
        $choose_List2 = 6 - $Choose_List1; //The total should be 6 cards
        $this->Chosen = array();

        for($i = 1; $i <= $Choose_List1; ++$i)
        {
            $index = random_int(1, count($List1));
            array_push($this->Chosen, $List1[$index - 1]);
            array_splice($List1, $index-1, 1);
        }
        for($i = 1; $i <= $choose_List2; ++$i)
        {
            $index = random_int(1, count($List2));
            array_push($this->Chosen, $List2[$index - 1]);
            array_splice($List2, $index-1, 1);
        }
    }
}



function RaiseErrorGEN($Errno)
{
    switch ($Errno)
    {
        case ERR_N:
             exit("Input for number of games is unvalid\n");
        default:
             exit("Undefined error occured\n");

    }
}



