<!DOCTYPE html>
<HTML>
<head>
<link rel = "stylesheet" type = "text/css" href = "css/style.css">
<?php 



session_start();

if(isset($_SESSION["c_id"]) && isset($_SESSION["c_store"]) 
         && isset($_SESSION["f_id"]) && isset($_SESSION["date"])) {
             $film = $_SESSION["f_id"];
             $user = $_SESSION["c_id"];
             $store = $_SESSION["c_store"];
             $dt = $_SESSION["date"];
         }
else {
        header("location: order.php");
}


require_once('MySQLWrap.php');
require_once ('header.php');
require_once('Films.php');


$conn =  new MySQLWrap();
$fl = new Films($conn);
$f = $fl->getFilmById($film);

$ret_date = new DateTime($dt);
$now_date = new DateTime(date("Y-m-d" , time()));
$diff = $now_date->diff($ret_date)->format("%a");

$pay = $f["rental_rate"] * ($diff / $f["rental_duration"]);
$_SESSION["pay"] = $pay;

$inv= $fl->getInventoryByfilm($f["film_id"], $store);
$_SESSION["inv"] = $inv["inventory_id"];


?>

</head>
<body>
    <br/>
<form action = "payprocess.php" method = "POST">
<p class="title"> Your Order </p>
<br/>
<label>Film: </label><strong> <?=$f["title"]?> </strong>
<br/><br/>
<label>Return: </label><strong> <?=$dt?> </strong>
<br/><br/>
<label>Duration: </label><strong> <?=$diff?> days </strong>
<br/><br/>
<label>Price: </label><strong> <?=$pay?> $ </strong>
<br/><br/><br/><br/>
<input name = "pay" value = "0" hidden>
<button> Pay </button>
</form>
</body>



</HTML>