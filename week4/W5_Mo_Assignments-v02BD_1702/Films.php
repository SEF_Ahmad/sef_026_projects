<?php
/**
 * Author: Ahmad Issa
 * Class: Films
 * Goal: Searh films and handle access to the films table
 */

require_once('MySQLWrap.php');

 class Films
 {
    private $db;


    //@Construct this take MySQLWrap object to use for data access
    function __Construct($connection)
    {
        $this->db = $connection;
    }


    //@getAllFilms return all films in film table 
    public function getAllFilms()
    {
        $query = "SELECT * FROM film";
        $result = $this->db->Execute($query);
        if(!$this->db->IsError()) {
            return $result;
        }
    }


    //@getUnrentedFilms it search for all films except the rented ones
    public function getUnrentedFilms($store)
    {
        $query = "SELECT * FROM film AS f WHERE f.film_id IN ".
        "(select i.film_id from inventory as i WHERE i.store_id = $store "
        ."AND i.inventory_id NOT IN (SELECT inventory_id FROM rental AS r " 
        ."WHERE  r.inventory_id = i.inventory_id AND r.return_date > CURDATE() ))"
        ." ";
        $result = $this->db->Execute($query);
        if(!$this->db->IsError()) {
           return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }


    //@getFilmById takes Id and return film of that id
    public function getFilmById($id)
    {
        $query = "SELECT * FROM film AS f WHERE f.film_id = $id ";
        $result = $this->db->Execute($query);
        if(!$this->db->IsError()) {
           return mysqli_fetch_assoc($result);
        }
    }


    public function getInventoryByfilm($id, $store)
    {
        $query = "SELECT * FROM inventory AS i WHERE i.store_id = $store "
        ." AND i.film_id = $id "
        ."AND i.inventory_id NOT IN (SELECT inventory_id FROM rental AS r " 
        . "WHERE r.inventory_id = i.inventory_id "
        ." AND r.return_date > CURDATE() )";
        $result = $this->db->Execute($query);
        if(!$this->db->IsError()) {
           return mysqli_fetch_assoc($result);
        }
    }
 }