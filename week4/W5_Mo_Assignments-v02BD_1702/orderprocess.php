<?php 

require_once('MySQLWrap.php');
require_once('Customers.php');
session_start();

if(isset($_POST["film_id"]) && isset($_POST["date"]) && $_POST["date"]!="") {
    $film_id = $_POST["film_id"];
    $dt = $_POST["date"];
}
else {
    $_SESSION["err"] = "Enter All needed data";
    header("location: order.php");
}

$ret_date = new DateTime($dt);
$now_date = new DateTime(date("Y-m-d" , time()));
$diff = $now_date->diff($ret_date)->format("%R%a");
$diff = (int)$diff;

if($diff < 0) {
    $_SESSION["err"] = "Date should be in future, we don't have time machine";
    header("location: order.php");
}
else {
    $_SESSION["f_id"] = $film_id;
    $_SESSION["date"] = $dt;
    header("location: pay.php");
}