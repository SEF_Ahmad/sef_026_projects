<?php
/**
 * Author: Ahmad Issa
 * Class: Rental
 * Goal: Store Rent info in rental, and pay info in payment
 */

 class Rental
 {
     private $db;
     

    //@Construct this take MySQLWrap object to use for data access
    function __Construct($connection)
    {
        $this->db = $connection;
    }


    //@Rent takes store ID, return date, customer ID, inventory ID, pay and process rent
    public function Rent($store, $ret, $c_id, $inv, $pay)
    {
        $nw = date("Y-m-d" , time());
        $staff = $this->getStaffByStore($store)["staff_id"];
        $query = "INSERT INTO rental (rental_date, inventory_id, customer_id, "
                ."return_date, staff_id) VALUES ('$nw', $inv, $c_id, "
                ." '$ret', $staff)";
        $result = $this->db->Execute($query);
        
        if(!$this->db->IsError()) {
            $r_id =  $this->db->getLastInsert();
            $result = $this->Pay($c_id, $staff, $r_id, $pay, $nw);
            if ($result) {
                return TRUE;
            }  
        }
        return FALSE;
    }


    //@Pay this function is private only executed by Rent after rent process.
    private function Pay($c_id, $staff, $rental, $amount, $dt)
    {
        $query = "INSERT INTO payment (customer_id, staff_id, rental_id, "
        ." amount, payment_date) VALUES ($c_id, $staff, $rental, $amount, "
        ." '$dt')";
        $result = $this->db->Execute($query);
        return !$this->db->IsError(); 
    }


    //@getStaffByStore given every store has specific staff, we return staff for given store
    private function getStaffByStore($store)
    {
        $query = "SELECT * FROM staff AS s WHERE s.store_id = $store";
        $result = $this->db->Execute($query);
        if(!$this->db->IsError()) {
            return mysqli_fetch_assoc($result);
        }
    }

 }