<?php 
session_start();
require_once('MySQLWrap.php');
require_once('Customers.php');
$conn = new MySQLWrap();
$_SESSION['conn'] = serialize($conn);
$customer = new Customers($conn);

if(isset($_POST["email"])) {
    $email = $_POST["email"];
}

if(isset($_POST["fname"])) {
    $fname = $_POST["fname"];
}

if(isset($_POST["lname"])) {
    $lname = $_POST["lname"];
}

if($email != "" && $lname != "" && $fname != "") {
    $result = $customer->Login($email, $fname, $lname);
    $_SESSION["c_id"] = $result["customer_id"];
    $_SESSION["c_store"] = $result["store_id"];
    header("location: order.php");
}
else {
    $_SESSION["err"] = "This cutomer is not available";
    header("location: login.php");
}