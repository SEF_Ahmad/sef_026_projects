<?php
/**
 * Author: Ahmad Issa
 * Class: MySQLWrap
 * Goal: Handle the database connection and queries
 */

require_once('conf.php');


class MySQLWrap
{

    private $conn;
    public $err;
    public $errno;
    

    //@Construct, attempt to create connection based on conf.php values.
    function __Construct()
    {
        $this->conn = mysqli_connect(DB_ADRS, DB_USER, DB_PSWRD, DB_NAME);
        if(!$this->conn) {
            $this->$err = "Error, in DB connect. ".mysqli_connect_error();
            $this->errno = mysqli_connect_errorno();
        }
        else {
            $this->err = "";
            $this->errno = 0;
        }
    }


    //@Execute it execute query for all other objects, it handles error.
    public function Execute($SQL)
    {
        $result = mysqli_query($this->conn, $SQL);
        if($result == FALSE) {
            $this->errno = -100;
            $this->err = "Error: Can't execute query:\n'$SQL'\n"
                           .mysqli_error($this->conn);
        }
        else {
            $this->err = "";
            $this->errno = 0;
            return $result;
        }
    }


    //@getLastInsert return the last inserted ID in the database (for this connection)
    public function getLastInsert()
    {
        return mysqli_insert_id ($this->conn);
    }


    //@IsError test function to know if error occured
    public function IsError()
    {
        if($this->errno != 0) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }


    //@destruct close the connection before the death of object
    function __destruct()
    {
        mysqli_close($this->conn);
    }
}