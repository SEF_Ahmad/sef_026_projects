<?php

session_start();
if(!isset($_POST["pay"])) {
    header("location: order.php");
}

if(isset($_SESSION["c_id"]) && isset($_SESSION["c_store"]) 
         && isset($_SESSION["f_id"]) && isset($_SESSION["date"]) 
         && isset($_SESSION["pay"]) && isset($_SESSION["inv"]) ) {

             $film = $_SESSION["f_id"];
             $user = $_SESSION["c_id"];
             $store = $_SESSION["c_store"];
             $dt = $_SESSION["date"];
             $inv = $_SESSION["inv"];
             $pay = $_SESSION["pay"];
         }
else {
        header("location: order.php");
}

require_once('MySQLWrap.php');
require_once('Rental.php');

$db = new MySQLWrap();
$rent = new Rental($db);
$result = $rent->Rent($store, $dt, $user, $inv, $pay);
if($result) {
    session_abort();
    session_start();
    $_SESSION["c_id"] = $user;
    $_SESSION["c_store"] = $store;
    header("location: report.php");
}
else {
    header("location: order.php");
}
