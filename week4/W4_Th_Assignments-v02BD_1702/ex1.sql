/**
 * Author: Ahmad Issa.
*/
DROP DATABASE IF EXISTS Legal;
CREATE DATABASE IF NOT EXISTS Legal;


#Start
USE Legal;

CREATE TABLE IF NOT EXISTS Claims
                                (
                                    claim_id INT PRIMARY KEY,
                                    patient_name VARCHAR(100)
                                );


CREATE TABLE IF NOT EXISTS Defendants
                                (
                                    claim_id INT,
                                    defendant_name VARCHAR(100),
                                    FOREIGN KEY (claim_id) REFERENCES Claims(claim_id)
                                );


CREATE TABLE IF NOT EXISTS LegalEvents
                                (
                                    claim_id INT,
                                    defendant_name VARCHAR(100),
                                    claim_status VARCHAR(10),
                                    change_date DATE,
                                    FOREIGN KEY (claim_id) REFERENCES Claims(claim_id)
                                );


CREATE TABLE IF NOT EXISTS ClaimStatusCodes
                                (
                                    claim_status VARCHAR(10),
                                    claim_status_desc VARCHAR(100),
                                    claim_seq INT
                                );



#Insert into claims table
DELIMITER $$
DROP PROCEDURE IF EXISTS SetClaims; 
 CREATE PROCEDURE SetClaims(cid INT, pname VARCHAR(100))
 BEGIN
    INSERT INTO Claims values (cid, pname);
 END$$
DELIMITER ;


#Insert into Defendants table
DELIMITER $$
DROP PROCEDURE IF EXISTS SetDef; 
 CREATE PROCEDURE SetDef(cid INT, dname VARCHAR(100))
 BEGIN
    INSERT INTO Defendants values (cid, dname);
 END$$
DELIMITER ;


#Insert into LegalEvents table
DELIMITER $$
DROP PROCEDURE IF EXISTS SetEvents; 
 CREATE PROCEDURE SetEvents(cid INT, dname VARCHAR(100),
                               status VARCHAR(10), dt DATE)
 BEGIN
    INSERT INTO LegalEvents values (cid, dname, status, dt);
 END$$
DELIMITER ;


#Insert into ClaimStatusCodes table
DELIMITER $$
DROP PROCEDURE IF EXISTS SetCsc; 
 CREATE PROCEDURE SetCsc(cd VARCHAR(10), csd VARCHAR(100), cs INT)
 BEGIN
    INSERT INTO ClaimStatusCodes values (cd, csd, cs);
 END$$
DELIMITER ;


#Populate table claims
call SetClaims(1, 'Bassem Dghaidi');
call SetClaims(2, 'Omar Breidi');
call SetClaims(3, 'Marwan Sawwan');

#Populate table Defendants
call SetDef(1, 'Jean Skaff');
call SetDef(1, 'Elie Meouchi');
call SetDef(1, 'Radwan Sameh');
call SetDef(2, 'Joseph Eid');
call SetDef(2, 'Paul Syoufi');
call SetDef(2, 'Radwan Sameh');
call SetDef(3, 'Issam Awwad');

#Populate table LegalEvents
call SetEvents(1, 'Jean Skaff', 'AP', '2016-01-01');
call SetEvents(1, 'Jean Skaff', 'OR', '2016-02-02');
call SetEvents(1, 'Jean Skaff', 'SF', '2016-03-01');
call SetEvents(1, 'Jean Skaff', 'CL', '2016-04-01');
call SetEvents(1, 'Radwan Sameh', 'AP', '2016-01-01');
call SetEvents(1, 'Radwan Sameh', 'OR', '2016-02-02');
call SetEvents(1, 'Radwan Sameh', 'SF', '2016-03-01');
call SetEvents(1, 'Elie Meouchi', 'AP', '2016-01-01');
call SetEvents(1, 'Elie Meouchi', 'OR', '2016-02-02');
call SetEvents(2, 'Radwan Sameh', 'AP', '2016-01-01');
call SetEvents(2, 'Radwan Sameh', 'OR', '2016-02-01');
call SetEvents(2, 'Paul Syoufi', 'AP', '2016-01-01');
call SetEvents(3, 'Issam Awwad', 'AP', '2016-01-01');


#Populate table ClaimStatusCodes
call SetCsc('AP', 'Awaiting review panel', 1);
call SetCsc('OR', 'Panel opinion rendered', 2);
call SetCsc('SF', 'Suit filed', 3);
call SetCsc('CL', 'Closed', 4);


#Solution:

SELECT 
    F.claim_id, F.patient_name, cs.claim_status
FROM
    (SELECT 
        S.claim_id, S.patient_name, MIN(S.max) AS min
    FROM
        (SELECT 
        l.claim_id,
            l.defendant_name,
            c.patient_name,
            MAX(cs.claim_seq) AS max
    FROM
        Claims AS c
    JOIN Defendants AS d ON c.claim_id = d.claim_id
    JOIN LegalEvents AS l ON l.claim_id = d.claim_id
    JOIN ClaimStatusCodes AS cs ON cs.claim_status = l.claim_status
    GROUP BY l.claim_id , l.defendant_name) AS S
    GROUP BY S.claim_id) AS F
        JOIN
    ClaimStatusCodes AS cs ON F.min = cs.claim_seq
ORDER BY F.claim_id;