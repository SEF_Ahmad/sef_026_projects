/**
 *Author: Ahmad Issa
*/
CREATE DATABASE IF NOT EXISTS FinanceDB;

CREATE TABLE IF NOT EXISTS 
                    FinanceDB.FiscalYearTable (
                                                Id int primary key AUTO_INCREMENT,
                                                fiscal_year text not null,
                                                start_date date not null,
                                                end_date date not null
                                              );

USE FinanceDB; 

#Fire on insert 
DROP TRIGGER IF EXISTS SafeInsert;                                           
DELIMITER $$
CREATE TRIGGER SafeInsert 
    BEFORE INSERT ON  FinanceDB.FiscalYearTable
    FOR EACH ROW
 BEGIN
    call Filter(new.start_date, new.end_date);
 END$$
DELIMITER ;

#Fire on update
DROP TRIGGER IF EXISTS SafeUpdate;                                           
DELIMITER $$
CREATE TRIGGER SafeUpdate
    BEFORE UPDATE ON  FinanceDB.FiscalYearTable
    FOR EACH ROW
 BEGIN
    call Filter(new.start_date, new.end_date);
 END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS Filter; 
 CREATE PROCEDURE Filter(start_date date, end_date date)
 BEGIN
    if(date_format(start_date, "%Y-%m-%d") 
       ||
       date_format(end_date, "%Y-%m-%d")) 
    then 
        set @Start_Year = EXTRACT(YEAR from start_date);
        set @End_Year = EXTRACT(YEAR from end_date);
        set @Start_month = EXTRACT(MONTH from start_date);
        set @End_month = EXTRACT(MONTH from end_date);
        set @Start_day = EXTRACT(DAY from start_date);
        set @End_day = EXTRACT(DAY from end_date);
        if(@Start_Year - @End_Year < 0) then
            call RaiseError('Err: Year diff is -ve, or end date');
        elseif (@Start_month - @End_month <> 1 
                AND
                @Start_month - @End_month <> 0) then 
            call RaiseError('Err: Month diff should be 12 month');
        elseif (DATEDIFF(start_date, end_date) <> 366) then
            call RaiseError('Err: this is not 365 day year');
        end if;
        
    else
        call RaiseError('Err: Unproper format of start date, or end date');
    end if;
 END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS RaiseError; 
 CREATE PROCEDURE RaiseError(txt TEXT)
 BEGIN
    #ROLLBACK;
    SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = txt;
 END$$
DELIMITER ;