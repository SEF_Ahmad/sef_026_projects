/**
 *Author: Ahmad Issa
*/
CREATE DATABASE IF NOT EXISTS HospitalRecords;

CREATE TABLE IF NOT EXISTS 
                HospitalRecords.AnestProcedures 
                (
                    proc_id int primary key AUTO_INCREMENT,
                    anest_name text not null,
                    start_time TIME not null,
                    end_time TIME not null
                );

USE HospitalRecords; 



#insert data:
DELIMITER $$
DROP PROCEDURE IF EXISTS SetData; 
 CREATE PROCEDURE SetData(id int, name text, s time, e time)
 BEGIN
    insert into HospitalRecords.AnestProcedures values (id, name, s, e);
 END$$
DELIMITER ;

#Solve
DELIMITER $$
DROP PROCEDURE IF EXISTS Solve; 
 CREATE PROCEDURE Solve()
 BEGIN
    DECLARE n INT DEFAULT 0;
    DECLARE i INT DEFAULT 0;
    SELECT COUNT(*) FROM HospitalRecords.AnestProcedures INTO n;
    SET i = 0;
    WHILE i < n DO 
        select ap.start_time, ap.end_time, ap.proc_id, ap.anest_name, proc_id
        from HospitalRecords.AnestProcedures as ap LIMIT i,1 INTO 
        @start, @end_, @id, @curr, @pid;



        SELECT COUNT(*) from HospitalRecords.AnestProcedures as ap
        WHERE ap.proc_id <> @pid AND ap.anest_name = @curr 
        AND ((ap.start_time Between @start and  @end_)
        or (ap.end_time Between @start and  @end_)
        or (ap.start_time < @start and ap.end_time > @end_)) INTO @max;
        select @id, @max;
        SET @max = 0;
        SET i = i + 1;
    END WHILE;
 END$$
DELIMITER ;


#Advanced Solve
DELIMITER $$
DROP PROCEDURE IF EXISTS advSolve; 
 CREATE PROCEDURE advSolve(INOUT counts int, searched varchar(1024), s time, e time)
 BEGIN
    DECLARE n INT DEFAULT 0;
    DECLARE i INT DEFAULT 0;
    DECLARE m INT DEFAULT 0;
    DECLARE j INT DEFAULT 0;
    SELECT COUNT(*) FROM HospitalRecords.AnestProcedures INTO n;
    SET i = 0;
    WHILE i < n DO 
        select ap.start_time, ap.end_time, ap.proc_id, ap.anest_name
        from HospitalRecords.AnestProcedures as ap LIMIT i,1 INTO 
        @start, @end_, @id, @curr;
        SET j = 0;
        SET counts = counts + 1;
        SET searched = concat (searched, ", #", @id, "# ");  
        advSolve()

        SELECT * from HospitalRecords.AnestProcedures as ap
        WHERE ap.proc_id <> @id AND ap.anest_name = @curr 
        AND ((ap.start_time > @start and ap.start_time < @end_)
        or (ap.end_time > @start and ap.end_time < @end_)
        or (ap.end_time > @start and ap.start_time < @end_));
        select @id, @max;
        SET @max = 0;
        SET i = i + 1;
    END WHILE;
 END$$
DELIMITER ;



call SetData(1, "Albert", "08:00", "11:00");
call SetData(2, "Albert", "09:00", "13:00");
call SetData(3, "Kamal", "08:00", "13:30");
call SetData(4, "Kamal", "09:00", "15:30");
call SetData(5, "Kamal", "10:00", "11:30");
call SetData(6, "Kamal", "12:30", "13:30");
call SetData(7, "Kamal", "13:30", "14:30");
call SetData(8, "Kamal", "18:30", "19:00");

call Solve();


