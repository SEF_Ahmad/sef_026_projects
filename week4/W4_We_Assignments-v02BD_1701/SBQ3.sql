/**
 * Author: Ahmad Issa 
*/

SELECT c.country ,count(*) 
FROM sakila.country As c JOIN sakila.city As ci
ON c.country_id = ci.country_id JOIN sakila.address As a 
ON a.city_id = ci.city_id
Group By c.country_id 
ORDER BY count(*) desc
limit 3;