/**
 * Author: Ahmad Issa 
*/

SELECT a.first_name FROM sakila.actor as a WHERE a.actor_id = 8 INTO @a8;


SELECT a.first_name, a.last_name
FROM sakila.actor As a 
WHERE a.first_name = @a8 AND a.actor_id <> 8
 
UNION ALL

SELECT c.first_name, c.last_name
FROM sakila.customer As c 
WHERE c.first_name = @a8;
