/**
 * Author: Ahmad Issa 
*/
SELECT count(*) 
FROM sakila.film As f JOIN sakila.language as l
ON f.language_id = l.language_id AND f.release_year = 2006
Group By f.language_id 
ORDER BY count(*) desc
limit 3;