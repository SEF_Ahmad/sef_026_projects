/**
 * Author: Ahmad Issa 
*/

SELECT c.name, count(*)
FROM sakila.category As c JOIN sakila.film_category As fc 
ON c.category_id = fc.category_id JOIN sakila.film As f 
ON f.film_id = fc.film_id 
Group By c.category_id
Having count(*) between 55 AND 65
ORDER BY count(*) desc