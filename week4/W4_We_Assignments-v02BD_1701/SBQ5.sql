/**
 * Author: Ahmad Issa 
*/

SELECT a.first_name, a.last_name, f.release_year 
FROM sakila.actor as a JOIN sakila.film_actor as fa
ON a.actor_id = fa.actor_id
JOIN sakila.film as f 
ON f.film_id = fa.film_id
WHERE f.title LIKE "%Corcodile%"
OR f.title LIKE "%Shark%"
ORDER BY a.last_name;