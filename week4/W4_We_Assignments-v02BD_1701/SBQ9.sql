/**
 * Author: Ahmad Issa 
*/


SET @YEAR = '2005';
SELECT c.first_name As'Name',
date_format(r.rental_date, "%Y")  As 'Year', 
count(*) As 'Count'
FROM sakila.rental As r JOIN sakila.customer As c 
ON c.customer_id = r.customer_id AND date_format(r.rental_date, "%Y") = @YEAR
Group By r.customer_id,
         date_format(r.rental_date, "%Y")
Order By count(*) Desc
limit 3;