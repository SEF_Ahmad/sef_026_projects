/**
 * Author: Ahmad Issa 
*/


SELECT date_format(r.rental_date, "%m") As 'Month',
       date_format(r.rental_date, "%Y") As 'Year',
       s.store_id As 'Store',
       sum(p.amount) As 'Total',
       AVG(p.amount) As 'Average'
FROM sakila.payment As p JOIN sakila.staff As s 
ON s.staff_id = p.staff_id JOIN sakila.rental As r 
ON r.rental_id = p.rental_id
Group by date_format(r.rental_date, "%m"),
		 date_format(r.rental_date, "%Y"),
         s.store_id;




/*
#Slightly different approach

DELIMITER $$
DROP PROCEDURE IF EXISTS Solve;  
CREATE PROCEDURE Solve()

Begin
    DROP TABLE IF EXISTS tbl;
	CREATE temporary TABLE tbl
	(
	 id int,
	 cid int,
	 sid int,
     rid int,
	 amount decimal(4,2),
	 pd datetime, #payment day
	 up datetime,
     mth int,
     yr int
	);
    Insert Into tbl
	SELECT *, date_format(p.payment_date, "%m"),
    date_format(p.payment_date, "%Y") 
    FROM sakila.payment As p; 
    
    SELECT s.store_id,tbl.mth, tbl.yr, sum(tbl.amount), AVG(tbl.amount) 
    FROM tbl join sakila.staff as s 

#call Solve();
*/

