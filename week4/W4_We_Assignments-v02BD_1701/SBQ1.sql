/**
 * Author: Ahmad Issa 
*/
SELECT concat(a.first_name, ' ', a.last_name) as Name, count(*) as Movies 
FROM sakila.film_actor As fa JOIN sakila.actor As a 
ON a.actor_id = fa.actor_id 
Group By fa.actor_id ;