#Assigment 1 - Ex 2
IsOk=1
TotalMem=$(free -m | awk '/^Mem:/{print $2}')
FreeMem=$(free -m | awk '/^Mem:/{print $3}')
UsedMemPercentage=$(( ($FreeMem*100) / $TotalMem))
if (( $UsedMemPercentage >= 80 ))
then
echo "ALARM: Virtual Memory at $UsedMemPercentage %"
IsOk=0
fi


d=$(df -k --output=source,pcent)

counter=0
index=0
for disk in ${dutil[@]}
do
counter=$((counter + 1))
if [[ $disk =~ ^[0-9]+$ ]]
then
if (( $disk >= 80 ))
then
index=$((counter-1))
echo "ALARM: Disk ${dutil[$index]} is at $disk"
IsOk=0
fi
fi
done
if ((IsOk==1))
then
echo "Everything is ok"
fi
