#!/usr/bin/php
<?php

#@Ahmad Issa
#@As2.ex1.php

$Err = 0;
$DirIn = "";

if($argc < 3)
{
    $Err = 5;
    goto ERROR;
}

for ($i=0; $i<$argc;++$i)
{
   if(strcmp(trim($argv[$i]), "-i") == 0)
   {
       if($i+1 < $argc)
       {
         $DirIn = $argv[$i + 1];
         break;
       }
       else 
       {
           $Err = 2;
           goto ERROR;
       }
   }
}

if(file_exists($DirIn)) 
{
    if(is_file ($DirIn))
    {
        $Err = 3;
        goto ERROR;
    }
    else
    {
       // just continue
    }
}
else
{
    $Err = 4;
    goto ERROR; 
}

#Ensure that dir ends with / character
if(substr($DirIn, -1, 1) != "/")
  $DirIn.="/";

echo "Files Within $DirIn: \n";
DeepScan($DirIn, 0);




#The actual function
function DeepScan($Dir, $level)
{
   if(is_dir($Dir)) 
     $Entries = array_diff(scandir($Dir), array('.', '..'));
   else
     return; 

   foreach($Entries as $e)
   {
       if(is_file("$Dir$e"))
       {
           echo str_repeat (" " , $level)."-".$e."\n";
       }
       else
       {
           $s = str_repeat (" " , $level);
           echo $s."#".$e."\n";
           if(substr($Dir.$e, -1, 1) != "/")
            $e.="/";
           DeepScan($Dir.$e, $level+2);
       }
   }
}
#---------------------------------------------------------

//echo " |\n |________F.txt";
ERROR:
switch($Err)
{
    case 1:
          break;
    case 2:
          echo "Error: You need to provide directory path";
          break;
    case 3:
          echo "Error: The dir provided is a real file, you should provide real dir\n";
          break;
    case 4:
          echo "Error: The dir provided does not exist\n";
          break;
    case 5:
          echo "Error: You should provide more arguments\n";
          break;

}
?>