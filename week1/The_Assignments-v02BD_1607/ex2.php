#!/usr/bin/php
<?php

#@Ahmad Issa
#@As2.ex2.php

define("LOG_PATH", "/var/log/apache2/access.log");
$ERROR = 0;
$lineno = 0;
$file = fopen(LOG_PATH, "r");
if(!$file)
{
    $ERROR = 1;
    goto ERROR;
}
while(($line = fgets($file)) != false)
{
    $lineno++;
    $line = trim($line);
    if($line == "")
     continue;
    preg_match("/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/", $line, $IP);
    if(count($IP) == 0)
    {
        #In case of IPv6
        preg_match("/^[ABCDEF0-9]{1,4}\.[ABCDEF0-9]{1,4}\.[ABCDEF0-9]{1,4}\.[ABCDEF0-9]{1,4}\.[ABCDEF0-9]{1,4}\.[ABCDEF0-9]{1,4}\.[ABCDEF0-9]{1,4}\.[ABCDEF0-9]{1,4}/", $line, $IP);
        if(count($IP) == 0)
         {
           $ERROR = 2;
           goto ERROR;
         }
    }

    //[27/Feb/2018:18:03:40 +0200]
    preg_match("/[0-9]{1,2}\/[A-za-z]{2,4}\/[0-9]{4}\:[0-9]{1,2}\:[0-9]{1,2}\:[0-9]{1,2}/", $line, $DT);
    if(count($DT)== 0)
    {
        $ERROR = 3;
        goto ERROR;
    }

    preg_match("/(\+|\-)[0-9]{4}/", $line, $zone);
    if(count($zone) == 0)
    {
        $ERROR = 5;
        goto ERROR;
    }

    preg_match('/\"(GET|POST|DELETE)\s\/.*\"\s[0-9]{2,4}\s/', $line, $method);
    if(count($method) == 0)
    {
        $ERROR = 4;
        goto ERROR;
    }

    preg_match("/[0-9]{3}$/", trim($method[0]), $status);

    $DTObj =  date_create_from_format ("d/M/Y:H:i:s" , $DT[0] , new DateTimeZone($zone[0])  );
    $outDT = $DTObj->format('l, F m Y : H-i-s');
    $method[0] = trim($method[0]);
    $outMethod = substr($method[0], 0, strlen($method[0])- 3);
    echo "$IP[0] -- $outDT -- $outMethod -- $status[0]\n";
}

if($lineno == 0)
{
    echo "The log file at ".LOG_PATH." is empty.";
}

//127.0.0.1 -- Wednesday, March 16 2016 : 15-21-13 -- "GET /icons/text.gif HTTP/1.1" -- 200 





ERROR:
switch($ERROR)
{
    case 1:
           echo "Error: Can't open this file.";
           break;
    case 2:
            echo "Error: Line $lineno is corrupted no IP available\n";
            break;
    case 3:
            echo "Error Line $lineno is corrupted no proper date-time available\n";
            break;
    case 4:
            echo "Error Line $lineno is corrupted no state available\n";
            break;
    case 5:
            echo "Error Line $lineno is corrupted no time zone available\n";
            break;
    default:
            fclose($file);
}









?>